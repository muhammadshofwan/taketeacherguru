package com.taketeacher.teacher;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import com.taketeacher.teacher.model.Guru;

public class AvaDetailActivity extends AppCompatActivity {

    ImageView detail_ava;
    Guru profils;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ava_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        /**
         toolbar.setNavigationIcon(R.drawable.ic_save_black_24dp);
         toolbar.setNavigationOnClickListener(
         new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        Toast.makeText(AvaDetailActivity.this, "Cie-cie disimpen nih yee", Toast.LENGTH_SHORT).show();
        }
        }
         ); **/

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // getSupportActionBar().setSelectedNavigationItem(R.drawable.ic_save_black_24dp);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            //id = extras.getInt("profil");
            profils= (Guru) extras.getSerializable("profil");
        }

        detail_ava = (ImageView) findViewById(R.id.detail_ava);
        Picasso.with(getApplicationContext())
                .load(profils.getImage_url())
                .into(detail_ava);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       // int id = item.getItemId();

         switch (item.getItemId()) {
         case android.R.id.home:
         finish();
         return true;
         }
/**
        if (id == R.id.action_save_image) {
            Toast.makeText(AvaDetailActivity.this, "Cie-cie disave masa wkwk", Toast.LENGTH_LONG).show();
            return true;
        }
        switch (id) {
            case android.R.id.home:
                finish();
                return true;

        } **/
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.menu_detail_ava, menu);
        return true;
    }
}
