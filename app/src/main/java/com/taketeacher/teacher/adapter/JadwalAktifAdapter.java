package com.taketeacher.teacher.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.taketeacher.teacher.DetailJadwalActivity;
import com.taketeacher.teacher.R;
import com.taketeacher.teacher.model.Jadwal;

/**
 * Created by DELL on 07/12/2017.
 */

public class JadwalAktifAdapter extends RecyclerView.Adapter<JadwalAktifAdapter.ViewHolder>  {
    private List<Jadwal> data;
    private Activity activity;

    public JadwalAktifAdapter(Activity activity, List<Jadwal> data) {
        this.data=data;
        this.activity = activity;
    }

    public JadwalAktifAdapter(Context applicationContext) {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_home_aktif, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Jadwal item = new Jadwal();
        item = this.data.get(i);
        viewHolder.lbl_id.setText(Integer.toString(item.getId()));
        viewHolder.txt_nama.setText(item.getMurid().getNamaLengkap());
       // viewHolder.txt_nama.setText(item.getNama_murid());
        viewHolder.txt_status.setText(item.getStatus_label());
        viewHolder.txt_lokasi.setText(item.getLokasi());




        String s = item.getTanggal();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat TanggalFormat = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat jamDateFormat = new SimpleDateFormat("HH:mm");
        try
        {
            Date date = simpleDateFormat.parse(s);
            String tanggal = TanggalFormat.format(date);
            String jam = jamDateFormat.format(date);

            viewHolder.txt_tanggal.setText(tanggal);


           // System.out.println("date : "+simpleDateFormat.format(date));
        }
        catch (ParseException ex)
        {
           // System.out.println("Exception "+ex);
        }
        /**
        MainMenu item = new MainMenu(activity);
        item = this.data.get(i);
        // Setting all values in listview
        viewHolder.lbl_id.setText(Integer.toString(item.getId_anggota()));
        //txt_nama.setText(FormatText.maxChar(FormatText.capital(item.get(nama)),20));
        viewHolder.lbl_nama.setText(item.getNama());
        viewHolder.lbl_noanggota.setText(item.getMcif());

        DecimalFormat df = new DecimalFormat();
        viewHolder.lbl_saldo.setText( FormatText.rupiahFormat(item.getTotalSaldo()));
        //viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(Integer.parseInt(item.get("tagihan"))));
        if(item.getTotalTagihan() > 0){
            viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(item.getTotalTagihan()));
            viewHolder.lbl_tunggakan.setVisibility(View.VISIBLE);
        }else{
            viewHolder.lbl_tunggakan.setVisibility(View.GONE);
        }
    **/

        final Jadwal finalItem = item;
        viewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = finalItem.getId();
                Intent intent = new Intent(activity, DetailJadwalActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",id);
                activity.getApplicationContext().startActivity(intent);
            }
        });
        viewHolder.btn_aktif_jadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = finalItem.getId();
                Intent intent = new Intent(activity, DetailJadwalActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",id);
                activity.getApplicationContext().startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_nama;
        private TextView lbl_id;
        private TextView txt_lokasi;
        private TextView txt_status;
        private TextView txt_tanggal;
        private TextView txt_jam;
        private ImageView img_menu;
        private Button btn_aktif_jadwal;
        private View container;

        public ViewHolder(View view) {
            super(view);
            txt_nama= (TextView) view.findViewById(R.id.nama_murid_jadwal);
            txt_lokasi= (TextView) view.findViewById(R.id.lokasi_mengajar);
            txt_status= (TextView) view.findViewById(R.id.status_aktif);
            lbl_id= (TextView) view.findViewById(R.id.lbl_id);
            txt_tanggal= (TextView) view.findViewById(R.id.tanggal_jadwal_aktif);
            txt_jam= (TextView) view.findViewById(R.id.waktu_jadwal);
            img_menu= (ImageView) view.findViewById(R.id.image_jadwal);
            btn_aktif_jadwal= (Button) view.findViewById(R.id.btn_home_aktif);
            container = view.findViewById(R.id.cv_aktif);
        }
    }
}
