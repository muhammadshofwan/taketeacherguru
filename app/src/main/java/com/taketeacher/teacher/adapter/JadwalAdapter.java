package com.taketeacher.teacher.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.taketeacher.teacher.DetailJadwalActivity;
import com.taketeacher.teacher.R;
import com.taketeacher.teacher.RatingActivity;
import com.taketeacher.teacher.model.Jadwal;

/**
 * Created by DELL on 07/12/2017.
 */

public class JadwalAdapter extends RecyclerView.Adapter<JadwalAdapter.ViewHolder>  {
    private List<Jadwal> data;
    private Activity activity;

    public JadwalAdapter(Activity activity, List<Jadwal> data) {
        this.data=data;
        this.activity = activity;
    }

    public JadwalAdapter(Context applicationContext) {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_jadwal, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Jadwal item = new Jadwal();
        item = this.data.get(i);
        viewHolder.lbl_id.setText(Integer.toString(item.getId()));
        viewHolder.txt_nama.setText(item.getMurid().getNamaLengkap());
       //viewHolder.txt_nama.setText(item.getNama_murid());
        viewHolder.txt_pelajaran.setText(item.getNama_pelajaran());
        viewHolder.txt_status.setText(item.getStatus_label());

        if(item.getStatus() == 1){
            viewHolder.btn_ulasan.setVisibility(View.GONE);
            viewHolder.txt_status.setBackgroundResource(R.drawable.status_bg_blue);
        }else if(item.getStatus() == 0){
            viewHolder.btn_ulasan.setVisibility(View.GONE);
            viewHolder.txt_status.setBackgroundResource(R.drawable.status_bg_pending);
        }else if(item.getStatus() == 5){
            viewHolder.txt_status.setBackgroundResource(R.drawable.status_bg_green);
            if (item.getRatingGuru() != null){
                viewHolder.ratingBar.setRating(item.getRatingGuru().getPoint());
                viewHolder.btn_ulasan.setVisibility(View.GONE);
                viewHolder.ratingBar.setVisibility(View.VISIBLE);
            }else{
                viewHolder.btn_ulasan.setVisibility(View.VISIBLE);
                viewHolder.ratingBar.setVisibility(View.GONE);
            }
        }else if(item.getStatus() == 4){
            viewHolder.btn_ulasan.setVisibility(View.GONE);
            viewHolder.txt_status.setBackgroundResource(R.drawable.status_bg_red);
           // viewHolder.container.setVisibility(View.GONE);
        }else if (item.getStatus()==3){
            viewHolder.txt_status.setText("Belum Diselesaikan Murid");
            viewHolder.btn_ulasan.setText("Chat Murid");
            viewHolder.txt_status.setBackgroundResource(R.drawable.status_bg_pending);
            viewHolder.btn_ulasan.setVisibility(View.VISIBLE);
        }

        String s = item.getTanggal();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat TanggalFormat = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat jamDateFormat = new SimpleDateFormat("HH:mm");
        try
        {
            Date date = simpleDateFormat.parse(s);
            String tanggal = TanggalFormat.format(date);
            String jam = jamDateFormat.format(date);

            viewHolder.txt_tanggal.setText(tanggal);
            viewHolder.txt_jam.setText(jam);


           // System.out.println("date : "+simpleDateFormat.format(date));
        }
        catch (ParseException ex)
        {
           // System.out.println("Exception "+ex);
        }

        Picasso.with(this.activity)
                .load(item.getMurid().getImage_url())
               .into(viewHolder.img_menu);

        final Jadwal finalItem = item;
        viewHolder.btn_ulasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (finalItem.getStatus()==5){
                    int id = finalItem.getId();
                    int idGuru = finalItem.getMurid().getId();
                    Intent intent = new Intent(activity, RatingActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("idJadwal", String.valueOf(id));
                    intent.putExtra("idmurid", String.valueOf(idGuru));
                    intent.putExtra("nama", finalItem.getMurid().getNamaLengkap());
                    intent.putExtra("gambar", finalItem.getMurid().getImage_url());
                    activity.getApplicationContext().startActivity(intent);
                }else if (finalItem.getStatus()==3){
                    String telpon = "+62" + finalItem.getMurid().getTelpon().substring(1);
                    String pesan = "Halo " + finalItem.getMurid().getNamaLengkap() ;
                    Uri uri = Uri.parse("http://api.whatsapp.com/send?phone="+telpon +"&text="+pesan);
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(uri);
                    activity.startActivity(sendIntent);
                }

            }
        });
        /**
        MainMenu item = new MainMenu(activity);
        item = this.data.get(i);
        // Setting all values in listview
        viewHolder.lbl_id.setText(Integer.toString(item.getId_anggota()));
        //txt_nama.setText(FormatText.maxChar(FormatText.capital(item.get(nama)),20));
        viewHolder.lbl_nama.setText(item.getNama());
        viewHolder.lbl_noanggota.setText(item.getMcif());

        DecimalFormat df = new DecimalFormat();
        viewHolder.lbl_saldo.setText( FormatText.rupiahFormat(item.getTotalSaldo()));
        //viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(Integer.parseInt(item.get("tagihan"))));
        if(item.getTotalTagihan() > 0){
            viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(item.getTotalTagihan()));
            viewHolder.lbl_tunggakan.setVisibility(View.VISIBLE);
        }else{
            viewHolder.lbl_tunggakan.setVisibility(View.GONE);
        }
    **/

       // final Jadwal finalItem = item;
        viewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = finalItem.getId();
                int idGuru = finalItem.getGuru_id();
                Intent intent = new Intent(activity, DetailJadwalActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",id);
                intent.putExtra("idGuru", idGuru);
                activity.getApplicationContext().startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_nama;
        private TextView lbl_id;
        private TextView txt_pelajaran;
        private TextView txt_status;
        private TextView txt_tanggal;
        private TextView txt_jam;
        private ImageView img_menu;
        private Button btn_ulasan;
        private RatingBar ratingBar;
        private View container;

        public ViewHolder(View view) {
            super(view);
            txt_nama= (TextView) view.findViewById(R.id.name_jadwal);
            txt_pelajaran= (TextView) view.findViewById(R.id.pelajaran_jadwal);
            txt_status= (TextView) view.findViewById(R.id.status_jadwal);
            lbl_id= (TextView) view.findViewById(R.id.lbl_id);
            txt_tanggal= (TextView) view.findViewById(R.id.tanggal_jadwal);
            txt_jam= (TextView) view.findViewById(R.id.waktu_jadwal);
            img_menu= (ImageView) view.findViewById(R.id.image_jadwal);
            ratingBar= (RatingBar) view.findViewById(R.id.rating_ulasan);
            btn_ulasan = (Button) view.findViewById(R.id.btn_ulasan);
            container = view.findViewById(R.id.cv_jadwal);
        }
    }
}
