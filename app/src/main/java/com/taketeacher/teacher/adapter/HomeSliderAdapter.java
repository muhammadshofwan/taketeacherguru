package com.taketeacher.teacher.adapter;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class HomeSliderAdapter extends SliderAdapter {

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        switch (position) {
            case 0:
                viewHolder.bindImageSlide("https://lh3.googleusercontent.com/TpqdmqDA6h06LfAnl7LfkL6JIDaA-czrL71ONu1W3jzQEHcN3CD0zMGAWhby3zmzEU8D3X8ZPvECfQpXpaj3kNyYWTX99MGI4stVIWsFzkysth9e4XNIgzKJzyTY0V9YBDPM8iTjRjz5S1yyzDtkNvzNQC5BRzocm3S6C6Zs-wW0yLE0vhLg0tb7QUWWaGbsxYedq2NIsWLga1wbwCsHRV7SjozsHAOnz3Qw3IVKJoGDcFYhk8jzbh7FLD3EDBbG_PCKjaOEVvIsXlAO6cS_mQDlnJd_OwWQzQ9syTjWKzeckH4N0CB7_TkC-0n1S7x_SlhENPKFz9kiglUBAACla4AczQmB5nEv6ynTc2MqmY3tM_rfnnMX5hl1qncizKvOKTlWPprnN-HFT89Ou7dQAu4D1Iwotwy3P2919peUaOsvXmG9bCH9zp3qVvPQsN0oLzIyWfOhSwgsPbVcGVFyZdBq97eyo4xrPUGWmOGDVjNntcquqymPU_4BFvc8wSys7aDCmfsNy3HkXrE0GjFGXHiKZmHWFtBZmpNvPx339SpC0NB9OJS51S75Uhudx7Qz=w1366-h647");
                break;
            case 1:
                viewHolder.bindImageSlide(       "https://lh3.googleusercontent.com/abCDL8NxSxDyft0VQeA5-dHzltEkvYjkmYiFhFgwVnAV0aS8OMmuGQmmSDkmBOj-zWeKl4c6OUuaEZQkjnRQR7uBWUnqYXrhvIyXAEMaS1DxTFqJHSATP66ln89cOEAWfz14kpYlKk7hpvg5gfYTC_5vD2JjhBY4wHhUbl7DOi-qaRM25RyjT0ca10MGPuHdzBp37ORSesEQNLDETSO6j4g_vT_h29cEFfOtH-sB1c3G-SMfXyDEYlwwZx2BdQ68TbJ1RF4K_javAnbxxh0RKq_4IvRddB_ZsRCGIH-G_AAPvnccecRaZeK85YuH6pwNsqKrOKOGLBJk1Z_31JN6o2Ncekl70vCsUblXVotLVIpycN3_f1xDQvylvBuc6QnQzLmG8zDF_arB8fWhaZFWb8rq01kD-YYSsN-7MD9UuruyTUoIKEJgra6e-5WKqe0XP48dNgsoHTLOBnYaXEqtMF77xuIZ8K4Jyf6lNPave8RD2R4WfcJqEOkAc3n-A8y5_4lRJxaOEn4uTR_fnNVKC8c2TqTwytselMySBW9cSDp9ZzvBUaZU6dqT7qtCOQgI=w1366-h647");
                break;
            case 2:
                viewHolder.bindImageSlide(      "https://lh3.googleusercontent.com/ZvUwL8SM1whl2QySKC1mWfOvthdJTnlrYGkOmx_px34NDkNmZcURy6QwZR-F1KJK9a_NR7LqRk7Sisj7UhBEbAWzSZQ8A38d_27Nd3ppp3ghfaQTKjFCRUhkKXRkcW7gFO6G4NdmExUuAoSHjlKjrHdV9urwYoBo95an-DLHkpVURHn8IyGzs2bHHRLvZQmDVebBN6ZEexsf37a3ED5OuwBVDrwK76RNS-IckiTFFq3-kQeJhwO-KOE0RgTCGtBu5gvjywW_4qJsZLGkNtoM6708vO4mamHpyYiqzylI3nedCozN-Xh8rD7020rWOCjlivGCf5eo3gIp3GJdLeYRYNce1IdQ3Hpdd-nOVb5I-HfCk31WbuHVSrkf9Eb_cRJLDSN3N7rnjYoLJQWRgFCYC1cYK9unxU_uY4pjgNw1v3MXqfsNgjQqYdtJF4g5Gyx7Q7WYJ9OjBGdjy-OMlgzoLwICk3w2LMrQmcb-X8FoHHXESJHyJcvnHXjEz3FZF4pkkw0s-aJW_-gA9egXBwq4yRA-07FaInVipkutgE0XcW99O-ufYIstplLajgY_bRl9=w1366-h647");
                break;
        }
    }
}
