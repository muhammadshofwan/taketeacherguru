package com.taketeacher.teacher.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.taketeacher.teacher.R;
import com.taketeacher.teacher.VerifikasiPembayaranActivity;
import com.taketeacher.teacher.model.Deposito;
import com.taketeacher.teacher.utils.FormatText;

/**
 * Created by DELL on 07/12/2017.
 */

public class DepositAdapter extends RecyclerView.Adapter<DepositAdapter.ViewHolder>  {
    private List<Deposito> data;
    private Activity activity;

    public DepositAdapter(Activity activity, List<Deposito> data) {
        this.data=data;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_deposit, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Deposito item = new Deposito();
        item = this.data.get(i);
        viewHolder.txt_teks.setText(item.getKeterangan());
        viewHolder.nominal.setText(FormatText.rupiahFormat(item.getJumlah()));


        String s = item.getTanggal();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat TanggalFormat = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat jamDateFormat = new SimpleDateFormat("HH:mm");
        try
        {
            Date date = simpleDateFormat.parse(s);
            String tanggal = TanggalFormat.format(date);
            String jam = jamDateFormat.format(date);

            viewHolder.txt_tanggal.setText(tanggal);
            viewHolder.txt_jam.setText(jam);


            // System.out.println("date : "+simpleDateFormat.format(date));
        }
        catch (ParseException ex)
        {
            // System.out.println("Exception "+ex);
        }


        //viewHolder.txt_tanggal.setText(item.getTanggal());
       // viewHolder.txt_jam.setText(item.getJam());
       // viewHolder.img_menu.setImageResource(item.getImage());
        /**
        MainMenu item = new MainMenu(activity);
        item = this.data.get(i);
        // Setting all values in listview
        viewHolder.lbl_id.setText(Integer.toString(item.getId_anggota()));
        //txt_nama.setText(FormatText.maxChar(FormatText.capital(item.get(nama)),20));
        viewHolder.lbl_nama.setText(item.getNama());
        viewHolder.lbl_noanggota.setText(item.getMcif());

        DecimalFormat df = new DecimalFormat();
        viewHolder.lbl_saldo.setText( FormatText.rupiahFormat(item.getTotalSaldo()));
        //viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(Integer.parseInt(item.get("tagihan"))));
        if(item.getTotalTagihan() > 0){
            viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(item.getTotalTagihan()));
            viewHolder.lbl_tunggakan.setVisibility(View.VISIBLE);
        }else{
            viewHolder.lbl_tunggakan.setVisibility(View.GONE);
        }
    **/


        final Deposito finalItem = item;
        viewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = finalItem.getId();
                Intent intent = new Intent(activity, VerifikasiPembayaranActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",id);
                activity.getApplicationContext().startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_teks;
        private ImageView img_menu;
        private TextView txt_tanggal;
        private TextView txt_jam;
        private TextView nominal;
        private TextView nominal_keluar;
        private View container;

        public ViewHolder(View view) {
            super(view);
            txt_teks= (TextView) view.findViewById(R.id.title_deposit);
            nominal= (TextView) view.findViewById(R.id.nominal);
            txt_tanggal= (TextView) view.findViewById(R.id.tanggal_deposit);
            txt_jam= (TextView) view.findViewById(R.id.jam_deposit);
           // nominal_keluar = (TextView) view.findViewById(R.id.nominal_keluar_deposit);
            //img_menu= (ImageView) view.findViewById(R.id.img_up_down);
            container = view.findViewById(R.id.cv_deposit);
        }
    }
}
