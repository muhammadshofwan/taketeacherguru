package com.taketeacher.teacher.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import com.taketeacher.teacher.R;
import com.taketeacher.teacher.model.Rating;

/**
 * Created by DELL on 07/12/2017.
 */

public class ListRatingAdapter extends RecyclerView.Adapter<ListRatingAdapter.ViewHolder>  {
    private List<Rating> data;
    private Activity activity;

    public ListRatingAdapter(Activity activity, List<Rating> data) {
        this.data=data;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_ulasan, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Rating item = new Rating();
        item = this.data.get(i);
        viewHolder.txt_teks.setText(item.getVoter_name());
       // viewHolder.nominal.setText("Rp." + (Integer.toString(item.getJumlah())));
         viewHolder.ratingBar.setRating(item.getPoint());
        viewHolder.komentar.setText(item.getKomentar());
       // viewHolder.txt_jam.setText(item.getJam());
       // viewHolder.img_menu.setImageResource(item.getImage());
        /**
        MainMenu item = new MainMenu(activity);
        item = this.data.get(i);
        // Setting all values in listview
        viewHolder.lbl_id.setText(Integer.toString(item.getId_anggota()));
        //txt_nama.setText(FormatText.maxChar(FormatText.capital(item.get(nama)),20));
        viewHolder.lbl_nama.setText(item.getNama());
        viewHolder.lbl_noanggota.setText(item.getMcif());

        DecimalFormat df = new DecimalFormat();
        viewHolder.lbl_saldo.setText( FormatText.rupiahFormat(item.getTotalSaldo()));
        //viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(Integer.parseInt(item.get("tagihan"))));
        if(item.getTotalTagihan() > 0){
            viewHolder.lbl_tunggakan.setText( FormatText.numberFormat(item.getTotalTagihan()));
            viewHolder.lbl_tunggakan.setVisibility(View.VISIBLE);
        }else{
            viewHolder.lbl_tunggakan.setVisibility(View.GONE);
        }
    **/
        Picasso.with(this.activity)
                .load(item.getVoter_image())
                .into(viewHolder.img_menu);

        // Deposito finalItem = item;
        viewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  int id = finalItem.getId();
               // Intent intent = new Intent(activity, DetailDepositActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //intent.putExtra("id",id);
                //activity.getApplicationContext().startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_teks;
        private RatingBar ratingBar;
        private ImageView img_menu;
        private TextView txt_tanggal;
        private TextView txt_jam;
        private TextView komentar;
        private View container;

        public ViewHolder(View view) {
            super(view);
            txt_teks= (TextView) view.findViewById(R.id.nama_ulasan);
            komentar= (TextView) view.findViewById(R.id.komentar);
            //txt_tanggal= (TextView) view.findViewById(R.id.tanggal_deposit);
            ratingBar= (RatingBar) view.findViewById(R.id.rating_ulasan);
            img_menu= (ImageView) view.findViewById(R.id.image_ulasan);
            container = view.findViewById(R.id.cv_ulasan);
        }
    }
}
