package com.taketeacher.teacher;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.taketeacher.teacher.model.Jadwal;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class DetailJadwalActivity extends AppCompatActivity  implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener   {
    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private GoogleApiClient googleApiClient;
    String currentDateTimeString;
    String tgl;
    Preference pref;
    final static int RQS_1 = 1;
    private TextView pelajaran,tanggal,jam,alamat,nama,status,keterangan_reschedule,durasi_belajar;
    private Button btn_reschedule,btn_batal,btn_acc,btn_start,btn_finish, btn_chat_gurru;
    private ImageView avatar,ic5;
    private LinearLayout container_button,background_detil;
    Jadwal jadwals;

    ProgressDialog progress ;
    int id;
    int idGuru;
    String info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_jadwal);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            id = extras.getInt("id");
            idGuru = extras.getInt("idGuru");
        }
        durasi_belajar = (TextView) findViewById(R.id.durasi_belajar);
        keterangan_reschedule = (TextView) findViewById(R.id.keterangan_reschedule);
        ic5 = (ImageView) findViewById(R.id.ic5);
        pelajaran = (TextView) findViewById(R.id.nama_siswa);
        tanggal = (TextView) findViewById(R.id.agama);
        //tanggal.setText(tgl);
        // SimpleDateFormat  format = new SimpleDateFormat("EEE, d MMM yyyy");
        /**
         Date date = new Date();
         try {

         date = format.parse(tgl);
         System.out.println(date);
         } catch (ParseException e) {
          TODO Auto-generated catch block
         e.printStackTrace();
         } **/
        btn_chat_gurru = (Button) findViewById(R.id.btn_chat_murid);
        btn_chat_gurru.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String telpon = "+62" + jadwals.getMurid().getTelpon().substring(1);
                // Uri uri = Uri.parse("smsto: " + "+62" + telpon.substring(1));
                // Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                // intent.setPackage("com.whatsapp");
                // startActivity(intent);
                String pesan;
                if (jadwals.getStatus() == 3){
                    pesan = "Halo " + jadwals.getMurid().getNamaLengkap() +" harap selesaikan jadwal saya diaplikasi Take Teacher";
                }else {
                    pesan = "Halo " + jadwals.getMurid().getNamaLengkap() ;
                }

                Uri uri = Uri.parse("http://api.whatsapp.com/send?phone="+telpon +"&text="+pesan);
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(uri);
                startActivity(sendIntent);

            }
        });

        jam = (TextView) findViewById(R.id.jam);
        alamat = (TextView) findViewById(R.id.alamat_detail);
        nama = (TextView) findViewById(R.id.nama_depan_guru);
        avatar = (ImageView) findViewById(R.id.ava_guru) ;
        status = (TextView) findViewById(R.id.status);
        container_button = (LinearLayout) findViewById(R.id.container_button);
        background_detil = (LinearLayout) findViewById(R.id.ll_detail_jadwal);

        progress = new ProgressDialog(this);
        //detailJadwal();

        btn_acc = (Button) findViewById(R.id.btn_acc);
        btn_acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                builder.setTitle("Konfirmasi");
                builder.setMessage("Anda yakin ingin accept jadwal ini?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                accMurid();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        btn_reschedule = (Button) findViewById(R.id.btn_reschedule);
        btn_reschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainIntent =  new Intent(getApplicationContext(),RescheduleActivity.class);
                mainIntent.putExtra("id",id);
                mainIntent.putExtra("idGuru",Integer.toString(idGuru));
                startActivity(mainIntent);

            }
        });
        btn_batal = (Button) findViewById(R.id.btn_batal);
        btn_batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                builder.setTitle("Konfirmasi");
                builder.setMessage("Anda yakin ingin membatalkan jadwal ini?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                batalMurid();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        btn_start = (Button) findViewById(R.id.btn_start);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                builder.setTitle("Konfirmasi");
                builder.setMessage("Anda yakin ingin memulai kelas pada jadwal ini?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                              /**  String tanggal = jadwals.getTanggal();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                Date strDate = null;
                                Date date= new Date();//Calendar.getInstance().getTime();
                                try {
                                    strDate = sdf.parse(tanggal);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                              //  String datenow = new SimpleDateFormat("yyyy-MM-dd").format(date);
                              //  String dateStart = new SimpleDateFormat("yyyy-MM-dd").format(strDate);
                                if (date.equals(strDate)) {

                                }else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                                    builder.setTitle("Perhatian");
                                    builder.setMessage("Tidak bisa memulai kelas.")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                   // batalMurid();

                                                }
                                            });
                                    // Create the AlertDialog object and return it
                                    AlertDialog dialog1 = builder.create();
                                    dialog1.show();
                                }**/

                                startBelajar();

                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });
        btn_finish = (Button) findViewById(R.id.btn_finish);
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                builder.setTitle("Konfirmasi");
                builder.setMessage("Anda yakin ingin menyelesaikan kelas pada jadwal ini?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                selesaiBelajar();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        detailJadwal();
    }


    void detailJadwal(){
        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "jadwals/"+id+"?";
        // host += "&id=" + subKategoriId;
        host += "expand=murid,jadwal&";
        host += "access-token=" + pref.getToken();
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.GET,
                host,
                Jadwal.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Loading Data");
        progress.setMessage("Loading Data...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<Jadwal> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Jadwal response) {
                progress.dismiss();
                Jadwal jadwal = response;
                jadwals = jadwal;
                nama.setText(jadwal.getMurid().getNamaLengkap());
                // tanggal.setText(jadwal.getTanggal().subSequence(0,11));
                durasi_belajar.setText(jadwal.getDurasi() + " jam");
                pelajaran.setText(jadwal.getNama_pelajaran());
                latitude = Double.parseDouble(jadwal.getLatitude());
                longitude = Double.parseDouble(jadwal.getLongitude());
                moveMap();

                /**
                 if(jadwal.getPelajaran_id() == 1){
                 Picasso.with(getApplicationContext())
                 .load("http://app.taketeacher.com/uploads/pelajaran/image/image_10631522147909.png")
                 .into((Target) background_detil);
                 }else if(jadwal.getStatus() == 2){
                 Picasso.with(getApplicationContext())
                 .load("http://app.taketeacher.com/uploads/pelajaran/image/image_36261522147972.png")
                 .into((Target) background_detil);
                 }else if(jadwal.getStatus() == 3){
                 Picasso.with(getApplicationContext())
                 .load("http://app.taketeacher.com/uploads/pelajaran/image/image_88621522148017.png")
                 .into((Target) background_detil);
                 } **/
                alamat.setText(jadwal.getLokasi());
                // alamat.setText(deposito.);
                status.setText(jadwal.getStatus_label());
                //jam.setText(jadwal.getTanggal().substring(11,19).subSequence(0,5));

                String s = jadwal.getTanggal();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                SimpleDateFormat TanggalFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
                SimpleDateFormat jamDateFormat = new SimpleDateFormat("HH:mm");
              //  SimpleDateFormat calendarFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
                try
                {
                    Date date = simpleDateFormat.parse(s);
                 //   Date datecal = calendarFormat.parse(s);
                    String tanggal_ = TanggalFormat.format(date);
                    String jam_ = jamDateFormat.format(date);

                   // Date dat1 = sdf.parse("Mon Mar 14 16:02:37 GMT 2011"));// all done
                   // Calendar cal = calendarFormat.getCalendar();
                   // String cal_ = calendarFormat.format(date);
                  //  cal.setTime(calendarFormat.parse(String.valueOf(calendarFormat)));

                    tanggal.setText(tanggal_);
                    jam.setText(jam_);
                   // setAlarm(cal);


                    // System.out.println("date : "+simpleDateFormat.format(date));
                }
                catch (ParseException ex)
                {
                    // System.out.println("Exception "+ex);
                }


                Picasso.with(getApplicationContext())
                        .load(jadwal.getMurid().getImage_url())
                        .into(avatar);


                if(jadwal.getStatus() == 1){
                    status.setBackgroundResource(R.drawable.status_bg_blue);
                    btn_acc.setVisibility(View.GONE);
                    btn_finish.setVisibility(View.GONE);
                    btn_batal.setVisibility(View.GONE);
                    btn_reschedule.setVisibility(View.GONE);
                }else if(jadwal.getStatus() == 0){
                    status.setBackgroundResource(R.drawable.status_bg_pending);
                    btn_start.setVisibility(View.GONE);
                    btn_finish.setVisibility(View.GONE);
                   btn_reschedule.setVisibility(View.GONE);
                }else if(jadwal.getStatus() == 2){
                    status.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    btn_start.setVisibility(View.GONE);
                    btn_acc.setVisibility(View.GONE);
                    btn_reschedule.setVisibility(View.GONE);
                    btn_batal.setVisibility(View.GONE);
                    btn_chat_gurru.setVisibility(View.GONE);
                }else if(jadwal.getStatus() == 5){
                    status.setBackgroundResource(R.drawable.status_bg_green);
                    container_button.setVisibility(View.GONE);
                    btn_reschedule.setVisibility(View.GONE);
                    btn_chat_gurru.setVisibility(View.GONE);
                }else if(jadwal.getStatus() == 4){
                    status.setBackgroundResource(R.drawable.status_bg_red);
                    container_button.setVisibility(View.GONE);
                }else if (jadwal.getStatus()==5){
                    status.setBackgroundResource(R.drawable.status_bg_green);
                    container_button.setVisibility(View.GONE);
                }else if (jadwal.getStatus() == 3){
                    status.setBackgroundResource(R.drawable.status_bg_pending);
                    status.setText("Belum Diselesaikan Murid");
                    btn_start.setVisibility(View.GONE);
                    btn_acc.setVisibility(View.GONE);
                    btn_reschedule.setVisibility(View.GONE);
                    btn_batal.setVisibility(View.GONE);
                    btn_finish.setVisibility(View.GONE);
                    btn_chat_gurru.setVisibility(View.VISIBLE);
                }

                keterangan_reschedule.setText(jadwal.getAlasan_reschedule_murid());
                if(jadwal.getAlasan_reschedule_murid().equals("-")){
                    ic5.setVisibility(View.GONE);
                    keterangan_reschedule.setVisibility(View.GONE);
                }else {
                    ic5.setVisibility(View.VISIBLE);
                    keterangan_reschedule.setVisibility(View.VISIBLE
                    );
                }



            }
        };
    }
/**
    private void setAlarm(Calendar cal){


        Intent intent = new Intent(getBaseContext(), DetailJadwalActivity.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
    } **/
    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();
            }
        };
    }


    void accMurid(){
        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "jadwals/acc/"+id+"?";
        // host += "&id=" + subKategoriId;
        //  host += "expand=murid,jadwal&";
        host += "access-token=" + pref.getToken();
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.POST,
                host,
                Jadwal.class,
                this.accMuridReqSuccessListener(),
                this.accMuridReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Accept Jadwal Murid");
        progress.setMessage("Loading..");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<Jadwal> accMuridReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Jadwal response) {

                progress.dismiss();
                info = "acc";
                Intent mainIntent =  new Intent(getApplicationContext(),InfoActivity.class);
                mainIntent.putExtra("info", info);
               // mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainIntent);
                finish();
                Toast.makeText(getApplicationContext(),
                        "Berhasil Accept",
                        Toast.LENGTH_LONG).show();

            }
        };
    }
    private Response.ErrorListener accMuridReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();

                progress.dismiss();

            }
        };
    }

    void batalMurid(){
        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "jadwals/cancel/"+id+"?";
        // host += "&id=" + subKategoriId;
        //  host += "expand=murid,jadwal&";
        host += "access-token=" + pref.getToken();
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.POST,
                host,
                Jadwal.class,
                this.batalMuridReqSuccessListener(),
                this.batalMuridReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Cancel Jadwal");
        progress.setMessage("Loading..");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<Jadwal> batalMuridReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Jadwal response) {

                progress.dismiss();
                info = "batal";
                Intent mainIntent =  new Intent(getApplicationContext(),InfoActivity.class);
                mainIntent.putExtra("info", info);
                startActivity(mainIntent);
                finish();
                Toast.makeText(getApplicationContext(),
                        "Cancel Jadwal",
                        Toast.LENGTH_LONG).show();


            }
        };
    }
    private Response.ErrorListener batalMuridReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();

            }
        };
    }

    void startBelajar(){
        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "jadwals/start/"+id+"?";
        // host += "&id=" + subKategoriId;
        //  host += "expand=murid,jadwal&";
        host += "access-token=" + pref.getToken();
        host += "&expand=transaksi";
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.POST,
                host,
                Jadwal.class,
                this.startMuridReqSuccessListener(),
                this.startMuridReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Mulai Belajar");
        progress.setMessage("Loading..");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<Jadwal> startMuridReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Jadwal response) {

                progress.dismiss();
                info = "mulai";
                Intent mainIntent =  new Intent(getApplicationContext(),InfoActivity.class);
                mainIntent.putExtra("info", info); startActivity(mainIntent);
                finish();
                Toast.makeText(getApplicationContext(),
                        "Selamat Mengajar",
                        Toast.LENGTH_LONG).show();



            }
        };
    }
    private Response.ErrorListener startMuridReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage("Waktu mulai belum sesuai dengan jadwal")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();

            }
        };
    }

    void selesaiBelajar(){
        String idselesai = String.valueOf(id);
        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "jadwals/finish/"+idselesai+"?";
        // host += "&id=" + subKategoriId;
        //  host += "expand=murid,jadwal&";
        host += "access-token=" + pref.getToken();
        host += "&expand=transaksi";
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.POST,
                host,
                Jadwal.class,
                this.finishMuridReqSuccessListener(),
                this.finishMuridReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Selesai Belajar");
        progress.setMessage("Loading..");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<Jadwal> finishMuridReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Jadwal response) {

                progress.dismiss();

                Jadwal jadwal = response;
                int idGuru = jadwals.getMurid().getId();
                int idJadwal = jadwals.getId();
                String nama = jadwals.getMurid().getNamaLengkap();
                String gambar = jadwals.getMurid().getImage_url();
                Intent mainIntent =  new Intent(getApplicationContext(),RatingActivity.class);
               // mainIntent.putExtra("jadwal",jadwals);
                mainIntent.putExtra("idJadwal", String.valueOf(idJadwal));
                mainIntent.putExtra("idmurid", String.valueOf(idGuru));
                mainIntent.putExtra("nama", nama);
                mainIntent.putExtra("gambar", gambar);

                startActivity(mainIntent);
                finish();
                Toast.makeText(getApplicationContext(),
                        "Selesai Mengajar",
                        Toast.LENGTH_LONG).show();
            }
        };
    }
    private Response.ErrorListener finishMuridReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage("Waktu selesai belum sesuai dengan jadwal")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailJadwalActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();

            }
        };
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            //mMap.setMyLocationEnabled(true);
        }

        mMap.getUiSettings().setAllGesturesEnabled(false);
        detailJadwal();
        // mMap.setOnMarkerDragListener(this);
        //mMap.setOnMapLongClickListener(this);

    }
    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }
    private void getCurrentLocation() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        //if (location != null) {
        //Getting longitude and latitude
        //longitude = location.getLongitude();
        //latitude = location.getLatitude();

        //moving the map to location
        //moveMap();
        //}
    }

    private void moveMap() {
        LatLng latLng = new LatLng(longitude, latitude);
        Log.d("lat", Double.toString(latitude));
        Log.d("lng", Double.toString(longitude));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        mMap.getUiSettings().setZoomControlsEnabled(false);

        MarkerOptions marker = new MarkerOptions().position(latLng);
        // Changing marker icon
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        // adding marker
        mMap.addMarker(marker);
    }
    @Override
    public void onClick(View v) {}

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

}
