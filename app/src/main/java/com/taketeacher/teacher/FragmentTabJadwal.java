package com.taketeacher.teacher;


import android.content.Context;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.taketeacher.teacher.model.Jadwal;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTabJadwal extends Fragment {


    ArrayList<Jadwal> jadwal;
    private ShimmerFrameLayout mShimmerViewContainer;
    ArrayList<Jadwal> dataJadwal = new ArrayList<Jadwal>();
    private RecyclerView rvJadwal;
    CharSequence Titles[]={"Pending","Akan datang","Selesai","Batal","Semua"};
    Context context;
    Preference pref;

  //  private DetailGuruActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    TabLayout tabs;
    ViewPager viewPager;

    JadwalAkanDatangFragment jadwalAkanDatangFragment;
    JadwalSelesaiFragment jadwalSelesaiFragment;
    JadwalBatalFragment jadwalBatalFragment;
    JadwalAllFragment jadwalAllFragment;
    JadwalPendingFragment jadwalPendingFragment;

    public FragmentTabJadwal() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pendidikan, container, false);
        viewPager = (ViewPager) v.findViewById(R.id.container);
        // Set Tabs inside Toolbar
        tabs = (TabLayout) v.findViewById(R.id.tabs);

        jadwalAkanDatangFragment = new JadwalAkanDatangFragment();
        jadwalSelesaiFragment = new JadwalSelesaiFragment();
        jadwalBatalFragment = new JadwalBatalFragment();
        jadwalAllFragment = new JadwalAllFragment();
        jadwalPendingFragment = new JadwalPendingFragment();

        downloadDataJadwal();

        return  v;
    }

    private void setupViewPager(ViewPager viewPager,List<Jadwal> jadwal) {
        FragmentTabJadwal.Adapter adapter = new FragmentTabJadwal.Adapter(getChildFragmentManager(), jadwal,Titles);


        jadwalAkanDatangFragment.jadwal = jadwal;
        jadwalSelesaiFragment.jadwal = jadwal;
        jadwalBatalFragment.jadwal = jadwal;
        jadwalAllFragment.jadwal = jadwal;
        jadwalPendingFragment.jadwal = jadwal;

        adapter.addFragment(jadwalPendingFragment, "Pending");
        adapter.addFragment(jadwalAkanDatangFragment, "Akan datang");
        adapter.addFragment(jadwalSelesaiFragment, "Selesai");
        adapter.addFragment(jadwalBatalFragment, "Batal");
        adapter.addFragment(jadwalAllFragment, "Semua");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private List<Jadwal> jadwal;
        CharSequence Titles[];

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        public Adapter(FragmentManager manager,List<Jadwal> jadwal, CharSequence mTitles[]) {
            super(manager);
            this.jadwal = jadwal;
            this.Titles = mTitles;
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }
    }

    void downloadDataJadwal(){
        pref = new Preference(getActivity().getApplicationContext());
        String host  = pref.getHostName() + "jadwal?";
        // host += "&id=" + idSubKategori;
        host += "expand=murid,jadwal&";
        host += "access-token=" + pref.getToken();
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.GET,
                host,
                Jadwal.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getActivity().getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);
    }

    private Response.Listener<Jadwal> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @Override
            public void onResponse(Jadwal response) {
                setupViewPager(viewPager,response.getJadwalList());
                tabs.setupWithViewPager(viewPager);
            }
        };
    }

    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
