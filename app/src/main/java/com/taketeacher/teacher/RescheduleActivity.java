package com.taketeacher.teacher;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.androidbuts.multispinnerfilter.MultiSpinner;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.taketeacher.teacher.model.Jadwal;
import com.taketeacher.teacher.model.KetersediaanGuru;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class RescheduleActivity extends AppCompatActivity {

    private EditText edt_tanggal,edt_jam,edt_keterangan;
    private Button btn_reschedule;
    private ImageButton btn_tanggal;
    private MultiSpinner multispinnerwaktu_harian;
    private final LinkedHashMap<String,Boolean> list_string=  new LinkedHashMap<String, Boolean>();
    private final List<String> list_value = new ArrayList<>();
    private final List<String> list_value_select = new ArrayList<>();
    private final List<String> list_value_string = new ArrayList<>();
    List<KetersediaanGuru> ketersediaanList = new ArrayList<>();
    KetersediaanGuru ketersediaanGuru;
    String jam;
    String tanggal_belajar;
    String keterangan;
    String idGuru;
    String tanggal;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    ProgressDialog progress ;
    String jam_label;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            id = extras.getInt("id");
            idGuru = extras.getString("idGuru");
        }

        progress = new ProgressDialog(this);
        edt_tanggal = (EditText) findViewById(R.id.tv_tanggal);
        edt_tanggal.setFocusable(false);

        edt_tanggal.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                //   getDataJam(guru_id,tanggal_awal,tanggal_akhir);
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                //  getDataJam(guru_id,tanggal_awal,tanggal_akhir);
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TextView myOutputBox = (TextView) findViewById(R.id.myOutputBox);
                //myOutputBox.setText(s);
                getDataJamHarian(idGuru);
            }
        });


        multispinnerwaktu_harian = (MultiSpinner) findViewById(R.id.multispinner_waktu_hari);

        btn_tanggal = (ImageButton) findViewById(R.id.btn_tanggal);
        btn_tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), KalenderActivity.class);
                intent.putExtra("guru_id",idGuru);
                startActivityForResult(intent, 1);
            }
        });

        edt_jam = (EditText) findViewById(R.id.tv_jam);
        edt_jam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);


                timePickerDialog = new TimePickerDialog(RescheduleActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        edt_jam.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                timePickerDialog.setTitle("Select Time");
                timePickerDialog.show();

            }
        });

        edt_keterangan= (EditText)findViewById(R.id.tv_keterangan);
        btn_reschedule = (Button) findViewById(R.id.btn_reschedule_jadwal);
        btn_reschedule.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(getApplicationContext(), PaymentFormActivity.class);
                //startActivity(intent);
//                guruId = edt_guru.getText().toString();
//                id_pelajaran = edt_pelajaran.getText().toString();
                jam = edt_jam.getText().toString();
                tanggal_belajar = edt_tanggal.getText().toString();
                keterangan = edt_keterangan.getText().toString();
                if (keterangan == null){
                    edt_keterangan.setText("-");
                }
                //total_tarif = edt_total.getText().toString();
                //lat = edt_lokasi.getText().toString();
                //lng = edt_lokasi.getText().toString();

                rescheduleJadwal();
            }
        });
    }

    void  getDataJamHarian(String guru_id){
        Preference pref = new Preference(getApplicationContext());
        //http://app.taketeacher.com/muridapi/jadwal/cekjadwalguru?access-token=z4seNW_yFMaK3tup4Ma_NNSbif56tpTt&id=2&tanggalmulai=2018-09-01&tanggalselesai=2018-09-30
        String host  ="http://app.taketeacher.com/muridapi/jadwal/cekketersediaanguru?";
        host += "access-token=" + pref.getToken();
        host += "&id="+idGuru;
        host += "&tanggalmulai="+tanggal;
        host += "&tanggalselesai="+tanggal;
        GsonRequest<KetersediaanGuru> jsObjRequest = new GsonRequest<KetersediaanGuru>(
                Request.Method.GET,
                host,
                KetersediaanGuru.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(RescheduleActivity.this.getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Loading");
        progress.setMessage("Cek Ketersediaan Guru...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<KetersediaanGuru> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<KetersediaanGuru>() {
            @Override
            public void onResponse(KetersediaanGuru response) {
                // compactCalendarView.removeAllEvents();
                progress.dismiss();
                list_value.clear();
                list_string.clear();
                KetersediaanGuru jadwal = response;
                ketersediaanGuru = jadwal;
                //JadwalAdapter adapter = new JadwalAdapter(ApplandeoActivity.this,response.getJadwalList());


                ketersediaanList = jadwal.getKetersediaanGuruList();
                if (tanggal != null) {
                    for (KetersediaanGuru item : jadwal.getKetersediaanGuruList()) {
                        ///  ketersediaanGuru = (KetersediaanGuru) item.getTanggal();
                        list_string.put(item.getWaktu_mulai().substring(0, 5) + " - " + item.getWaktu_selesai().substring(0, 5), false);
                        list_value.add(item.getId());
                    }
                }

                multispinnerwaktu_harian.setItems(list_string, new MultiSpinnerListener() {
                    @Override
                    public void onItemsSelected(boolean[] booleans) {
                        //  selected_jam = 0;

                        List<String> array = new ArrayList<String>(list_string.keySet());

                        for(int i=0; i<booleans.length; i++) {
                            if(booleans[i]) {
                                list_value_select.add(list_value.get(i));
                                list_value_string.add(array.get(i));
                                Log.i(",", String.valueOf(list_value_select));

                                jam_label = list_value_string.toString().substring(1,6);
                            }
                        }

                        //  edt_jmlh_jam.setText(String.valueOf(selected_jam));
                        edt_jam.setText(jam_label + "");
                    }
                });

            }
        };
    }
    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();
            }
        };
    }


    void rescheduleJadwal(){
        Preference pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "jadwals/reschedule/"+id+"?";
        // host += "&id=" + subKategoriId;
        host += "access-token=" + pref.getToken();
        // host += "&expand=guru,kategori,pelajaran,subKategori,murid";
        // host += "&page=1";

        Map<String,String> params = new HashMap<String, String>();
        params.put("Jadwal[tanggal]",tanggal+" "+jam_label);
        //  params.put("Jadwal[keterangan]",keterangan);
        params.put("Jadwal[alasan_reschedule_murid]",keterangan);


        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.POST,
                host,
                Jadwal.class,
                params,
                this.downloadDataPelajaranReq1SuccessListener(),
                this.downloadDataPelajaranReq1ErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);


        progress.setTitle("Reschedule Jadwal");
        progress.setMessage("Loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

    }
    private Response.Listener<Jadwal> downloadDataPelajaranReq1SuccessListener() {
        return new Response.Listener<Jadwal>() {
            @Override
            public void onResponse(Jadwal response) {
                progress.dismiss();
                String info = "reschedule";
                Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
                intent.putExtra("info",info);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),
                        "Berhasil Reschedule Jadwal",
                        Toast.LENGTH_LONG).show();

            }
        };
    }
    private Response.ErrorListener downloadDataPelajaranReq1ErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();

            }
        };
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                tanggal=data.getStringExtra("tanggal");
                // tanggal_start=data.getStringExtra("tanggal_start");
                //  tanggal_finish=data.getStringExtra("tanggal_finish");
                //  tanggal_awal=data.getStringExtra("tanggal_awal");
                //  tanggal_akhir=data.getStringExtra("tanggal_akhir");
                idGuru=data.getStringExtra("guru_id");
                edt_tanggal.setText(tanggal);
                // edt_tanggal_awal.setText(tanggal);
                // edt_tanggal_akhir.setText(tanggal_finish);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

}
