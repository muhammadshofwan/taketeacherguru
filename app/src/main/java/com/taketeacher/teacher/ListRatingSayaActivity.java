package com.taketeacher.teacher;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.io.UnsupportedEncodingException;

import com.taketeacher.teacher.adapter.ListRatingAdapter;
import com.taketeacher.teacher.model.Guru;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class ListRatingSayaActivity extends AppCompatActivity {

    Preference pref;
    RecyclerView rv_rating_saya;
    private ShimmerFrameLayout mShimmerViewContainer2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_rating_saya);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mShimmerViewContainer2 = findViewById(R.id.shimmer_view_container2);

        rv_rating_saya =(RecyclerView) findViewById(R.id.rv_rating_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_rating_saya.setLayoutManager(mLayoutManager);
        rv_rating_saya.setHasFixedSize(true);

        rv_rating_saya.setVisibility(View.GONE);

        downloadListRating();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void downloadListRating(){
        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "user/profile?";
        // host += "&id=" + idSubKategori;
        host += "access-token=" + pref.getToken();
        host += "&sort=-tanggal";
        host += "&expand=ratings";
        GsonRequest<Guru> jsObjRequest = new GsonRequest<Guru>(
                Request.Method.GET,
                host,
                Guru.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

//        mShimmerViewContainer1.startShimmerAnimation();
        //      mShimmerViewContainer1.setVisibility(View.VISIBLE);

        mShimmerViewContainer2.startShimmerAnimation();
        mShimmerViewContainer2.setVisibility(View.VISIBLE);

    }

    private Response.Listener<Guru> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Guru>() {
            @Override
            public void onResponse(Guru response) {

                mShimmerViewContainer2.stopShimmerAnimation();
                mShimmerViewContainer2.setVisibility(View.GONE);

                ListRatingAdapter adapter = new ListRatingAdapter(ListRatingSayaActivity.this,response.getRatings());
                rv_rating_saya.setAdapter(adapter);
                rv_rating_saya.setVisibility(View.VISIBLE);


            }
        };
    }

    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListRatingSayaActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListRatingSayaActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListRatingSayaActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListRatingSayaActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListRatingSayaActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();

                //  mShimmerViewContainer1.stopShimmerAnimation();
                // mShimmerViewContainer1.setVisibility(View.GONE);

                mShimmerViewContainer2.stopShimmerAnimation();
                mShimmerViewContainer2.setVisibility(View.GONE);
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();

    }
    @Override
    public void onPause() {
        mShimmerViewContainer2.stopShimmerAnimation();
        mShimmerViewContainer2.setVisibility(View.GONE);
        super.onPause();
    }
}
