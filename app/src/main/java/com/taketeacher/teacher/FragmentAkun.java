package com.taketeacher.teacher;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.taketeacher.teacher.model.Guru;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAkun extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public FragmentAkun() {
        // Required empty public constructor
    }
    String strDate;
    Preference pref;
    Context context;
    Guru profils;
    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private SupportMapFragment mapFragment;
    private GoogleApiClient googleApiClient;
    private RatingBar ratingBar;
    Activity activity;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout layout_profil, layout_koneksi;
    private ImageView img_ava, img_agama;
    private LinearLayout profile_container, layout_maps;
    private ShimmerFrameLayout mShimmerViewContainer;
    private Button btn_refresh,btn_profile,btn_logout;
    private TextView nama_depan_txt,nama_belakang_txt,jenis_kelamin_txt,alamat_txt,tanggal_lahir_txt,email_txt,telpon_txt,password_txt,main_nama_depan,main_nama_belakang,tgl_lahir, txt_agama;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_akun, container, false);


        mShimmerViewContainer =(ShimmerFrameLayout) v.findViewById(R.id.shimmer_view_container);
        profile_container =(LinearLayout) v.findViewById(R.id.profile_container);
        layout_maps =(LinearLayout) v.findViewById(R.id.layout_maps);

        //Bundle extras = getIntent().getExtras();
        //if (extras != null){
        // id = extras.getInt("id");
        // guru= (Guru) extras.getSerializable("guru");

        //  profils= (ProfilMurid) extras.getSerializable("guru");
        //}
        btn_refresh = (Button) v.findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(FragmentAkun.this).attach(FragmentAkun.this).commit();
            }
        });
        layout_koneksi = (LinearLayout) v.findViewById(R.id.layout_koneksi);
        layout_profil = (LinearLayout) v.findViewById(R.id.linearLayout_profil);
        nama_depan_txt = (TextView) v.findViewById(R.id.nama_siswa);
        nama_belakang_txt = (TextView) v.findViewById(R.id.nama_belakang_biodata);
        jenis_kelamin_txt = (TextView) v.findViewById(R.id.jam);
        alamat_txt = (TextView) v.findViewById(R.id.alamat_akun);
        tanggal_lahir_txt = (TextView) v.findViewById(R.id.tempat_lahir_akun);
        email_txt = (TextView) v.findViewById(R.id.email_akun);
        telpon_txt = (TextView) v.findViewById(R.id.telpon_akun);
        password_txt = (TextView) v.findViewById(R.id.password_akun);
        tgl_lahir = (TextView) v.findViewById(R.id.tanggal_lahir_akun);
        main_nama_depan = (TextView) v.findViewById(R.id.main_nama_depan);
        main_nama_belakang = (TextView) v.findViewById(R.id.main_nama_belakang);
        txt_agama = (TextView) v.findViewById(R.id.agama_akun);
        ratingBar = (RatingBar) v.findViewById(R.id.rating_bar_guru);
        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), ListRatingSayaActivity.class);
                    //   intent.putExtra("rating",  (ArrayList<Rating>) response.getRatings());
                    startActivity(intent);
                }
                return true;
            }
        });
        img_agama = (ImageView) v.findViewById(R.id.ic8);

        img_ava = (ImageView) v.findViewById(R.id.ava_foto);
        img_ava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(getActivity().getApplicationContext(), AvaDetailActivity.class);
                intent.putExtra("profil",profils);
                startActivity(intent);
            }
        });

        Button btn_pay = (Button) v.findViewById(R.id.btn_ubah_password);
        btn_pay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), UbahPasswordActivity.class);
                startActivity(intent);
            }
        });

         btn_profile = (Button) v.findViewById(R.id.btn_ubah_profile);
        btn_profile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(getActivity().getApplicationContext(), InputProfileActivity.class);
                //startActivity(intent);
                Intent intent = new Intent(getActivity().getApplicationContext(), InputProfileActivity.class);
                intent.putExtra("profil",profils);
                intent.putExtra("source", "profil");
                // intent.putExtra("guru", (Serializable) detail.getGuru());
                startActivity(intent);
            }
        });

         btn_logout = (Button) v.findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pref = new Preference(getActivity().getApplicationContext());
                pref.setToken("");
                Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();

            }
        });

        mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map_profil);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        profile_container.setVisibility(View.INVISIBLE);
        layout_maps.setVisibility(View.INVISIBLE);
        btn_profile.setVisibility(View.INVISIBLE);
        btn_logout.setVisibility(View.INVISIBLE);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        return  v;
    }

    void dataProfilMurid(){
        pref = new Preference(getActivity().getApplicationContext());
        String host  = pref.getHostName() + "user/profile?";
        // host += "&id=" + idSubKategori;
        host += "access-token=" + pref.getToken();
        // host += "&page=1";
        GsonRequest<Guru> jsObjRequest = new GsonRequest<Guru>(
                Request.Method.GET,
                host,
                Guru.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getActivity().getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        layout_koneksi.setVisibility(View.GONE);
    }
    private Response.Listener<Guru> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Guru>() {
            @Override
            public void onResponse(Guru response) {

                Guru profil = response;
                profils = profil;
                latitude = Double.parseDouble(profil.getLat());
                longitude = Double.parseDouble(profil.getLng());
                moveMap();
                main_nama_depan.setText(profil.getNama_depan());
                main_nama_belakang.setText(profil.getNama_belakang());
                nama_depan_txt.setText(profil.getNama_depan());
                nama_belakang_txt.setText(profil.getNama_belakang());
                alamat_txt.setText(profil.getAlamat());
                telpon_txt.setText(profil.getTelpon());
                email_txt.setText(profil.getEmail());
                tanggal_lahir_txt.setText(profil.getTempat_lahir());
                //tgl_lahir.setText(profil.getTanggal_lahir());
                ratingBar.setRating(Float.parseFloat(profil.getRating()));
                String s = profil.getTanggal_lahir();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat TanggalFormat = new SimpleDateFormat("dd MMMM yyyy");
                try
                {
                    Date date = simpleDateFormat.parse(s);
                    String tanggal_ = TanggalFormat.format(date);

                    tgl_lahir.setText(tanggal_);


                    // System.out.println("date : "+simpleDateFormat.format(date));
                }
                catch (ParseException ex)
                {
                    // System.out.println("Exception "+ex);
                }
                jenis_kelamin_txt.setText(profil.getJenis_kelamin_label());
                txt_agama.setText(profil.getAgama_label());
                Picasso.with(getActivity().getApplicationContext())
                        .load(profil.getImage_url())
                        .into(img_ava);

                if(profil.getAgama() == 1){
                    Picasso.with(getActivity().getApplicationContext())
                            .load(R.drawable.ic_agama_islam)
                            .into(img_agama);
                }else if(profil.getAgama() == 2){
                    Picasso.with(getActivity().getApplicationContext())
                            .load(R.drawable.ic_agama_kristen)
                            .into(img_agama);
                }else if(profil.getAgama() == 3){
                    Picasso.with(getActivity().getApplicationContext())
                            .load(R.drawable.ic_agama_kristen)
                            .into(img_agama);
                }else if(profil.getAgama() == 4){
                    Picasso.with(getActivity().getApplicationContext())
                            .load(R.drawable.ic_hindu)
                            .into(img_agama);
                }else if(profil.getAgama() == 5){
                    Picasso.with(getActivity().getApplicationContext())
                            .load(R.drawable.ic_buddha)
                            .into(img_agama); }


                mShimmerViewContainer.stopShimmerAnimation();

                mShimmerViewContainer.setVisibility(View.GONE);
                profile_container.setVisibility(View.VISIBLE);
                layout_maps.setVisibility(View.VISIBLE);
                btn_profile.setVisibility(View.VISIBLE);
                btn_logout.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                //  nama_belakang_txt.setText(profil.get());
                // jumlah.setText("Rp. " + Integer.toString(profil.getJumlah()));


            }
        };
    }
    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    layout_profil.setVisibility(View.GONE);
                    layout_koneksi.setVisibility(View.VISIBLE);
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();

                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                profile_container.setVisibility(View.VISIBLE);
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
        profile_container.setVisibility(View.INVISIBLE);
        layout_maps.setVisibility(View.INVISIBLE);
        btn_profile.setVisibility(View.INVISIBLE);
        btn_logout.setVisibility(View.INVISIBLE);
        dataProfilMurid();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            //mMap.setMyLocationEnabled(true);
        }

        mMap.getUiSettings().setAllGesturesEnabled(false);
        dataProfilMurid();
        // mMap.setOnMarkerDragListener(this);
        //mMap.setOnMapLongClickListener(this);

    }
    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }
    private void getCurrentLocation() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        //if (location != null) {
        //Getting longitude and latitude
        //longitude = location.getLongitude();
        //latitude = location.getLatitude();

        //moving the map to location
        //moveMap();
        //}
    }

    private void moveMap() {
        LatLng latLng = new LatLng(longitude, latitude);
        Log.d("lat", Double.toString(latitude));
        Log.d("lng", Double.toString(longitude));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        mMap.getUiSettings().setZoomControlsEnabled(false);

        MarkerOptions marker = new MarkerOptions().position(latLng);
        // Changing marker icon
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        // adding marker
        mMap.addMarker(marker);
    }
    @Override
    public void onClick(View v) {}

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onRefresh() {
        profile_container.setVisibility(View.INVISIBLE);
        layout_maps.setVisibility(View.INVISIBLE);
        btn_profile.setVisibility(View.INVISIBLE);
        btn_logout.setVisibility(View.INVISIBLE);
        dataProfilMurid();
    }
}
