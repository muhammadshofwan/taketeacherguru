package com.taketeacher.teacher;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.taketeacher.teacher.model.Deposito;
import com.taketeacher.teacher.model.Payment;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class TambahDepositActivity extends AppCompatActivity {

    Preference pref;
    private Spinner spnPaymentMethod;
    private Button btn_tambah_deposit;
    private EditText nominal_deposit_edt, edt_keterangan;
    ArrayList<Payment> paymentList;
    String jumlah;
    String metode;
    String keterangan;
    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_deposit);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        progress = new ProgressDialog(this);



        nominal_deposit_edt = (EditText) findViewById(R.id.jml_deposit);
        edt_keterangan = (EditText) findViewById(R.id.keterangan_deposit);
        spnPaymentMethod = (Spinner) findViewById(R.id.spinner_deposit);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, bank);
        spnPaymentMethod.setPrompt("-Metode Pembayaran-");
        spnPaymentMethod.setAdapter(adapter);
        btn_tambah_deposit = (Button) findViewById(R.id.btn_tambah_deposit);
        btn_tambah_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumlah = nominal_deposit_edt.getText().toString();
                keterangan = edt_keterangan.getText().toString();
                if (keterangan == null){
                    edt_keterangan.setText("Top up deposit Teacher Pay");
                }

                metode = Integer.toString(spnPaymentMethod.getSelectedItemPosition());

                postDeposit();
            }
        });
        // downloadDataPaymentMethod();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private static final String[] bank = new String[] {
            "","Mandiri", "BNI", "BCA", "BRI", "CIMB Niaga", "Permata"
    };

    void postDeposit(){
        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "deposits?";
        // host += "&id=" + subKategoriId;
        host += "access-token=" + pref.getToken();
        Map<String,String> params = new HashMap<String, String>();
        params.put("jumlah",jumlah);
        params.put("metode",metode);
        params.put("keterangan",keterangan);


        GsonRequest<Deposito> jsObjRequest = new GsonRequest<Deposito>(
                Request.Method.POST,
                host,
                Deposito.class,
                params,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Top Up Deposit");
        progress.setMessage("Loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

    }
    private Response.Listener<Deposito> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Deposito>() {
            @Override
            public void onResponse(Deposito response) {

                progress.dismiss();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                /**
                 AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                 builder.setTitle("Perhatian");
                 builder.setMessage("Selamat Anda Berhasil Daftar, silahkan masukkan profil anda")
                 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog, int id) {

                 }
                 });
                 // Create the AlertDialog object and return it
                 AlertDialog dialog = builder.create();
                 dialog.show(); **/
                finish();
                Toast.makeText(getApplicationContext(),
                        "Berhasil Menambah Deposit T-Pay",
                        Toast.LENGTH_LONG).show();


            }
        };
    }

    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(TambahDepositActivity.this);
                builder.setTitle("Perhatian");
                builder.setMessage("Cek kembali data anda")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        };
    }


    /**
     void downloadDataPaymentMethod(){
     pref = new Preference(getApplicationContext());
     String host  = pref.getHostName() + "payment-method?";
     host += "access-token=" + pref.getToken();
     GsonRequest<Payment> jsObjRequest = new GsonRequest<Payment>(
     Request.Method.GET,
     host,
     Payment.class,
     this.downloadDataPelajaranReqSuccessListener(),
     this.downloadDataPelajaranReqErrorListener());
     jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
     DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
     // Adding request to request queue
     AppController controller = new AppController(getApplicationContext());
     controller.addToRequestQueue(jsObjRequest);
     }

     private Response.Listener<Payment> downloadDataPelajaranReqSuccessListener() {
     return new Response.Listener<Payment>() {
    @Override
    public void onResponse(Payment response) {
    paymentList = response.getPaymentList();
    ArrayAdapter<Payment> adapter = new ArrayAdapter<Payment>(getApplicationContext(), R.layout.spinner_item, paymentList);
    adapter.setDropDownViewResource(R.layout.spinner_item);
    spnPaymentMethod.setAdapter(adapter);
    }
    };
     }

     private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
     return new Response.ErrorListener() {
    @Override
    public void onErrorResponse(VolleyError error) {
    String errorDescription = "Error download data Sub Kategori:\r\n";
    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
    // jika tidak ada jaringan
    errorDescription = errorDescription + "Tidak ada koneksi internet.";
    } else if (error instanceof AuthFailureError) {
    // jika user tidak diizinkan
    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
    } else if (error instanceof ServerError) {
    // jika error diserver
    errorDescription = errorDescription + "Error server.\r\n";
    } else if (error instanceof NetworkError) {
    // jika jaringan error
    errorDescription = errorDescription + "Error jaringan.\r\n";
    } else if (error instanceof ParseError) {
    // jika tidak dapat menemukan alamat
    errorDescription = errorDescription + "Error parse url.\r\n";
    }

    String body = "";
    if(error.networkResponse != null){
    if(error.networkResponse.data!=null) {
    try {
    body = new String(error.networkResponse.data,"UTF-8");
    } catch (UnsupportedEncodingException e) {
    e.printStackTrace();
    }
    }
    }
    errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
    }
    };
     }
     **/
}
