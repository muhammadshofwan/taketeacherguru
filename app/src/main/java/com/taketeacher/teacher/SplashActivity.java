package com.taketeacher.teacher;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.taketeacher.teacher.utils.Preference;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Preference pref = new Preference(getApplicationContext());
                pref.setHostName("http://app.taketeacher.com/guruapi/");
                String token;
                token = pref.getToken();
                if(token.equals("")){
                    Intent intens = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(intens);
                    finish();
                }else{
                    Intent intens = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intens);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);
    }
}
