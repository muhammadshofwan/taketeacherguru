package com.taketeacher.teacher;


import android.content.Context;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.Slider;
import com.taketeacher.teacher.adapter.HomeSliderAdapter;
import com.taketeacher.teacher.adapter.JadwalAdapter;
import com.taketeacher.teacher.adapter.JadwalAktifAdapter;
import com.taketeacher.teacher.model.Jadwal;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.PicassoImageLoadingService;
import com.taketeacher.teacher.utils.Preference;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    public FragmentHome() {
        // Required empty public constructor
    }
    private Slider slider;
    Context context;
    View last_object;
    JadwalAktifAdapter adapter;
    JadwalAdapter adapter_pending;
    ArrayList<Jadwal> jadwals = new ArrayList<Jadwal>();
    private RecyclerView rv_jadwal, rv_jadwal_pending;
    private TextView tv_view, tv_view1;
    private ConstraintLayout constraintLayout;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        this.context = v.getContext();

        Slider.init(new PicassoImageLoadingService(getActivity()));
        slider = (Slider) v.findViewById(R.id.slider);
        slider.setAdapter(new HomeSliderAdapter());

        last_object = slider;

        tv_view = (TextView) v.findViewById(R.id.tv_view);

        rv_jadwal =(RecyclerView) v.findViewById(R.id.rv_home_aktif);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rv_jadwal.setLayoutManager(mLayoutManager);
        rv_jadwal.setHasFixedSize(true);

        tv_view1 = (TextView) v.findViewById(R.id.tv_view1);
        constraintLayout = (ConstraintLayout) v.findViewById(R.id.profile_container1);
        rv_jadwal_pending =(RecyclerView) v.findViewById(R.id.rv_jadwal_pending);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(context);
        rv_jadwal_pending.setLayoutManager(mLayoutManager1);
        rv_jadwal_pending.setHasFixedSize(true);

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        downloadDataJadwal();
                                    }
                                }
        );

        return v;
    }

    void downloadDataJadwal(){
       Preference pref = new Preference(getActivity().getApplicationContext());
        String host  = pref.getHostName() + "jadwal?";
        // host += "&id=" + idSubKategori;
        host += "expand=murid,jadwal";
        host += "&sort=-transaksi_id";
        host += "&access-token=" + pref.getToken();
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.GET,
                host,
                Jadwal.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getActivity().getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);
    }

    private Response.Listener<Jadwal> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @Override
            public void onResponse(Jadwal response) {


                List<Jadwal> data = new ArrayList<Jadwal>();
                for(Jadwal item: response.getJadwalList()){
                    if(item.getStatus() ==  2){
                        data.add(item);
                    }
                }
                List<Jadwal> data1 = new ArrayList<Jadwal>();
                for(Jadwal item: response.getJadwalList()){
                    if(item.getStatus() ==  0){
                        data1.add(item);
                    }
                }

                adapter = new JadwalAktifAdapter(getActivity(),data);
                rv_jadwal.setAdapter(adapter);

                adapter_pending = new JadwalAdapter(getActivity(), data1);
                rv_jadwal_pending.setAdapter(adapter_pending);

                if (adapter.getItemCount() == 0){
                    tv_view.setVisibility(View.VISIBLE);
                    tv_view.setText("Tidak Ada Jadwal Aktif");
                }else {
                    tv_view.setVisibility(View.GONE);

                }

                if (adapter_pending.getItemCount() == 0){
                    constraintLayout.setVisibility(View.GONE);
                }
                swipeRefreshLayout.setRefreshing(false);

            }
        };
    }

    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                swipeRefreshLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public void onRefresh() {
        downloadDataJadwal();
    }
}
