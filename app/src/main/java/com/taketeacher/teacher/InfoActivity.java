package com.taketeacher.teacher;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class InfoActivity extends AppCompatActivity {

    String info;
    private TextView txt_judul,txt_isi,txt_isi_second;
    private ImageView img_info,img_selesai;
    private Button btn_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            info = extras.getString("info");
        }
        txt_judul = (TextView) findViewById(R.id.txt_info_jadwal_judul);
        txt_isi = (TextView) findViewById(R.id.txt_info_jadwal_isi);
        txt_isi_second = (TextView) findViewById(R.id.txt_info_jadwal_second);

        img_info = (ImageView) findViewById(R.id.img_info_jadwal);
        img_selesai = (ImageView) findViewById(R.id.img_info_selesai);

        btn_ok = (Button) findViewById(R.id.btn_ok_info);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });



        if (info.equals("acc")){
            String acc_sukses_judul = getResources().getString(R.string.jadwal_acc_judul);
            txt_judul.setText(Html.fromHtml(acc_sukses_judul));

            String acc_sukses_text = getResources().getString(R.string.jadwal_acc_isi);
            txt_isi.setText(Html.fromHtml(acc_sukses_text));

            Picasso.with(this)
                    .load(R.drawable.checked)
                    .into(img_info);

        }else if(info.equals("batal")){
            String batal_sukses_judul = getResources().getString(R.string.jadwal_batal_judul);
            txt_judul.setText(Html.fromHtml(batal_sukses_judul));

            String batal_sukses_text = getResources().getString(R.string.jadwal_batal_isi);
            txt_isi.setText(Html.fromHtml(batal_sukses_text));

            Picasso.with(this)
                    .load(R.drawable.cancel)
                    .into(img_info);

        }else if(info.equals("mulai")){
            String jadwal_mulai_judul = getResources().getString(R.string.jadwal_mulai_judul);
            txt_judul.setText(Html.fromHtml(jadwal_mulai_judul));


            Picasso.with(this)
                    .load(R.drawable.like_guru)
                    .into(img_info);

        }else if(info.equals("selesai")){
            String jadwal_selesai_judul = getResources().getString(R.string.jadwal_selesai_judul);
            txt_judul.setText(Html.fromHtml(jadwal_selesai_judul));

            String jadwal_selesai_sc = getResources().getString(R.string.jadwal_selesai_isi);
            txt_isi_second.setText(Html.fromHtml(jadwal_selesai_sc));

            Picasso.with(this)
                    .load(R.drawable.like_guru)
                    .into(img_info);

            img_selesai.setVisibility(View.VISIBLE);

        }else if(info.equals("reschedule")){
            String reschedule_judul = getResources().getString(R.string.jadwal_reschedule_judul);
            txt_judul.setText(Html.fromHtml(reschedule_judul));

            String reschedule_text = getResources().getString(R.string.jadwal_reschedule_isi);
            txt_isi.setText(Html.fromHtml(reschedule_text));

            Picasso.with(this)
                    .load(R.drawable.reschedule)
                    .into(img_info);
        }
    }
}
