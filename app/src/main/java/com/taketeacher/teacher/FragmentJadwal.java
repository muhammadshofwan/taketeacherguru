package com.taketeacher.teacher;


import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.taketeacher.teacher.adapter.JadwalAdapter;
import com.taketeacher.teacher.model.Jadwal;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentJadwal extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ShimmerFrameLayout mShimmerViewContainer2;
    private RecyclerView rvJadwal;
    Context context;
    Preference pref;
    DatePickerDialog datePickerDialog;
    private FloatingActionButton fab_filter;
    private ConstraintLayout constraintLayout;
    PopupWindow popupWindow;
    private TextView tv_view;
    String tanggal_awal;
    String tanggal_akhir;
    int status;
    private LinearLayout layout_kosong, layout_koneksi;
    private Button btn_refresh, btn_filter;
    private SwipeRefreshLayout swipeRefreshLayout;
    public FragmentJadwal() {
        // Required empty public constructor
    }


  //  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_jadwal, container, false);
        mShimmerViewContainer2 = v.findViewById(R.id.shimmer_view_container2);
        this.context = v.getContext();

        constraintLayout = (ConstraintLayout) v.findViewById(R.id.layout_constrait);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            constraintLayout.getForeground().setAlpha(0);
        }

        btn_refresh = (Button) v.findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(FragmentJadwal.this).attach(FragmentJadwal.this).commit();
            }
        });

        layout_kosong = (LinearLayout) v.findViewById(R.id.layout_kosong);
        layout_koneksi = (LinearLayout) v.findViewById(R.id.layout_koneksi);

        rvJadwal =(RecyclerView) v.findViewById(R.id.rv_jadwal);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvJadwal.setLayoutManager(mLayoutManager);
        rvJadwal.setHasFixedSize(true);

        tv_view = (TextView) v.findViewById(R.id.tv_view);
        rvJadwal.setVisibility(View.GONE);

        fab_filter = (FloatingActionButton) v.findViewById(R.id.fab_filter);
        btn_filter = (Button) v.findViewById(R.id.btn_filter);
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DisplayMetrics dm = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

                int width = dm.widthPixels;
                int height = dm.heightPixels;

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View customView = inflater.inflate(R.layout.popup_layout,null);

                //instantiate popup window
                popupWindow = new PopupWindow(
                        customView,
                        ConstraintLayout.LayoutParams.MATCH_PARENT,
                        ConstraintLayout.LayoutParams.WRAP_CONTENT,
                        true
                );

                popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onDismiss() {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            constraintLayout.getForeground().setAlpha(0);
                        }
                    }
                });
                popupWindow.setOutsideTouchable(true);

                // Set an elevation value for popup window
                // Call requires API level 21
                if(Build.VERSION.SDK_INT>=21){
                    popupWindow.setElevation(5.0f);
                }
                //display the popup window
                // popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);

                final Spinner spinnerStatus = (Spinner) customView.findViewById(R.id.spinner_status);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, statusList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerStatus.setAdapter(adapter);
                final EditText tanggalAwal = (EditText) customView.findViewById(R.id.tanggal_awal);
                tanggalAwal.setFocusable(false);
                tanggalAwal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR); // current year
                        int mMonth = c.get(Calendar.MONTH); // current month
                        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                        // date picker dialog
                        datePickerDialog = new DatePickerDialog(context,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        String strYear, strMonth, strDay;
                                        // set day of month , month and year value in the edit text
                                        strYear = Integer.toString(year);
                                        if(monthOfYear < 9){
                                            strMonth = "0" + Integer.toString(monthOfYear+1);
                                        }else{
                                            strMonth = Integer.toString(monthOfYear+1);
                                        }

                                        if(dayOfMonth < 10){
                                            strDay = "0" + Integer.toString(dayOfMonth);
                                        }else{
                                            strDay = Integer.toString(dayOfMonth);
                                        }

                                        tanggalAwal.setText(strYear + "-" + strMonth + "-" + strDay);

                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();
                    }
                });

                final EditText tanggalAkhir = (EditText) customView.findViewById(R.id.tanggal_akhir);
                tanggalAkhir.setFocusable(false);
                tanggalAkhir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR); // current year
                        int mMonth = c.get(Calendar.MONTH); // current month
                        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                        // date picker dialog
                        datePickerDialog = new DatePickerDialog(context,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        String strYear, strMonth, strDay;
                                        // set day of month , month and year value in the edit text
                                        strYear = Integer.toString(year);
                                        if(monthOfYear < 9){
                                            strMonth = "0" + Integer.toString(monthOfYear+1);
                                        }else{
                                            strMonth = Integer.toString(monthOfYear+1);
                                        }

                                        if(dayOfMonth < 10){
                                            strDay = "0" + Integer.toString(dayOfMonth);
                                        }else{
                                            strDay = Integer.toString(dayOfMonth);
                                        }

                                        tanggalAkhir.setText(strYear + "-" + strMonth + "-" + strDay);

                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();
                    }
                });

                final Button btn_cari = (Button) customView.findViewById(R.id.btn_cari);
                btn_cari.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rvJadwal.setAdapter(null);
                        tanggal_awal = tanggalAwal.getText().toString();
                        tanggal_akhir = tanggalAkhir.getText().toString();
                        status = spinnerStatus.getSelectedItemPosition();
                        downloadListJadwal(status,tanggal_awal,tanggal_akhir,"tanggal");
                        popupWindow.dismiss();
                    }
                });

                tanggalAwal.setText(tanggal_awal);
                tanggalAkhir.setText(tanggal_akhir);
                spinnerStatus.setSelection(status);
                popupWindow.showAtLocation(constraintLayout, Gravity.BOTTOM,0,0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    constraintLayout.getForeground().setAlpha(220);
                }
            }
        });

        Date today = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        Date firstDayOfMonth = calendar.getTime();

        calendar.add(Calendar.YEAR, 1);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.add(Calendar.DATE, -1);


        Date lastDayOfMonth = calendar.getTime();

        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        tanggal_awal = sdf.format(firstDayOfMonth);
        tanggal_akhir = sdf.format(lastDayOfMonth);
        status = 0;

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        return v;
    }

    private static final String[] statusList = new String[] {
            "Semua","Pending","Akan Datang","Mulai","Selesai","Batal"
    };

    void downloadListJadwal(int status, String tanggalAwal, String tanggalAkhir, String sort){
        pref = new Preference(getActivity().getApplicationContext());
        String host  = pref.getHostName() + "jadwals?";
        // host += "&id=" + idSubKategori;
        host += "expand=murid,jadwal";
        if(status > 0 ){
            status = status - 1;
            host += "&JadwalSearch[status]="+status;
        }

        host += "&JadwalSearch[tanggalAwal]="+tanggalAwal;
        host += "&JadwalSearch[tanggalAkhir]="+tanggalAkhir;
        host += "&sort="+sort;
       // host += "&sort=-transaksi_id";
        host += "&sort=-tanggal";
        host += "&access-token=" + pref.getToken();
        GsonRequest<Jadwal> jsObjRequest = new GsonRequest<Jadwal>(
                Request.Method.GET,
                host,
                Jadwal.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getActivity().getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

//        mShimmerViewContainer1.startShimmerAnimation();
        //      mShimmerViewContainer1.setVisibility(View.VISIBLE);

        mShimmerViewContainer2.startShimmerAnimation();
        mShimmerViewContainer2.setVisibility(View.VISIBLE);
        layout_kosong.setVisibility(View.GONE);
        layout_koneksi.setVisibility(View.GONE);

    }

    private Response.Listener<Jadwal> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Jadwal>() {
            @Override
            public void onResponse(Jadwal response) {

                mShimmerViewContainer2.stopShimmerAnimation();
                mShimmerViewContainer2.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                JadwalAdapter adapter = new JadwalAdapter(getActivity(),response.getJadwalList());
                rvJadwal.setAdapter(adapter);
                rvJadwal.setVisibility(View.VISIBLE);

                if (adapter.getItemCount() == 0){
                    layout_kosong.setVisibility(View.VISIBLE);
                }else {
                    layout_kosong.setVisibility(View.GONE);

                }
            }
        };
    }

    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    layout_koneksi.setVisibility(View.VISIBLE);
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();

                //  mShimmerViewContainer1.stopShimmerAnimation();
                // mShimmerViewContainer1.setVisibility(View.GONE);

                mShimmerViewContainer2.stopShimmerAnimation();
                mShimmerViewContainer2.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public void onResume() {
        rvJadwal.setVisibility(View.GONE);
        downloadListJadwal(status,tanggal_awal,tanggal_akhir, "tanggal");
        super.onResume();

    }
    @Override
    public void onPause() {
        mShimmerViewContainer2.stopShimmerAnimation();
        mShimmerViewContainer2.setVisibility(View.GONE);
        super.onPause();
    }


    @Override
    public void onRefresh() {
        rvJadwal.setVisibility(View.GONE);
        downloadListJadwal(status,tanggal_awal,tanggal_akhir, "tanggal");

    }
}
