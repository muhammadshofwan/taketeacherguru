package com.taketeacher.teacher;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.taketeacher.teacher.model.Jadwal;
import com.taketeacher.teacher.model.Rating;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class RatingActivity extends AppCompatActivity {

    private TextView txt_nama_murid,txt_status;
    private ImageView ava_murid;
    private RatingBar ratingBar;
    private EditText edt_message;
    private Button btn_submit;
    ProgressDialog progress;
    String point;
    String komentar;
    String user_id;
    String idGuru;
    String idJadwal;
    Jadwal jadwals;
    String nama;
    String gambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
                //jadwals = (Jadwal) extras.getSerializable("jadwal");
            idGuru = extras.getString("idmurid");
            idJadwal = extras.getString("idJadwal");
            gambar = extras.getString("gambar");
            nama = extras.getString("nama");
        }


        progress = new ProgressDialog(this);
        txt_nama_murid = (TextView) findViewById(R.id.nama_murid);
        txt_status = (TextView) findViewById(R.id.status);
        txt_status.setBackgroundResource(R.drawable.button_bg_green);
        ava_murid = (ImageView) findViewById(R.id.ava_murid);
        edt_message = (EditText) findViewById(R.id.comment_rating);
        ratingBar = (RatingBar) findViewById(R.id.rating_input);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // point = getCallingPackage().getBytes();
               // point = ratingBar.;

                point = Integer.toString((int) ratingBar.getRating());
                if (point.equals("0")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(RatingActivity.this);
                    builder.setTitle("Perhatian");
                    builder.setMessage("Harap untuk memberi rating pada murid !")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else {
                    komentar = edt_message.getText().toString();
                    postRating();
                }

            }
        });

        Picasso.with(getApplicationContext())
                .load(gambar)
                .into(ava_murid);
        txt_nama_murid.setText(nama);
       // txt_status.setText(jadwals.getStatus_label());
    }

    void postRating(){
        Preference pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "ratings?";
        // host += "&id=" + subKategoriId;
        host += "access-token=" + pref.getToken();
         host += "&expand=murid";
        // host += "&page=1";

        Map<String,String> params = new HashMap<String, String>();
        //params.put("Rating[user_id]", idGuru);
        params.put("Rating[user_id]", idGuru);
        params.put("Rating[jadwal_id]", idJadwal);
        params.put("Rating[point]", point);
        params.put("Rating[komentar]",komentar);
        //params.put("Rating[jenis]",alamat);


        GsonRequest<Rating> jsObjRequest = new GsonRequest<Rating>(
                Request.Method.POST,
                host,
                Rating.class,
                params,
                this.downloadDataPelajaranReq1SuccessListener(),
                this.downloadDataPelajaranReq1ErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Memberikan Ulasan");
        progress.setMessage("Loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<Rating> downloadDataPelajaranReq1SuccessListener() {
        return new Response.Listener<Rating>() {
            @Override
            public void onResponse(Rating response) {
                progress.dismiss();
                String info = "selesai";
                Intent mainIntent =  new Intent(getApplicationContext(),InfoActivity.class);
                mainIntent.putExtra("info", info);
                startActivity(mainIntent);
                finish();

                Toast.makeText(getApplicationContext(),
                        "Terima kasih",
                        Toast.LENGTH_LONG).show();

            }
        };
    }
    private Response.ErrorListener downloadDataPelajaranReq1ErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(RatingActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(RatingActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(RatingActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(RatingActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(RatingActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                progress.dismiss();
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();

            }
        };
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(RatingActivity.this).setTitle("Perhatian")
                .setMessage("Tidak bisa meninggalkan halaman ini, harap beri rating terlebih dahulu kepada murid.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }
}
