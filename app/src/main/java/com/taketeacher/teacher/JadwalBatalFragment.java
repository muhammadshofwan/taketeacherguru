package com.taketeacher.teacher;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.taketeacher.teacher.adapter.JadwalAdapter;
import com.taketeacher.teacher.model.Jadwal;
import com.taketeacher.teacher.utils.Preference;


/**
 * A simple {@link Fragment} subclass.
 */
public class JadwalBatalFragment extends Fragment {


    private RecyclerView rv_jadwal;
    Preference pref;
    Context context;
    int id;
    public List<Jadwal> jadwal;
    public JadwalBatalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_jadwal_batal, container, false);

        rv_jadwal =(RecyclerView) v.findViewById(R.id.rv_jadwal);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rv_jadwal.setLayoutManager(mLayoutManager);
        rv_jadwal.setHasFixedSize(true);

        List<Jadwal> data = new ArrayList<Jadwal>();
        for(Jadwal item: jadwal){
            if(item.getStatus() ==  4){
                data.add(item);
            }
        }

        JadwalAdapter adapter = new JadwalAdapter(getActivity(),data);
        rv_jadwal.setAdapter(adapter);
        return v;
    }


}
