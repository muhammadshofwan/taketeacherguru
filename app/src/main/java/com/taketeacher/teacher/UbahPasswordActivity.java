package com.taketeacher.teacher;

import android.app.ProgressDialog;
import android.content.DialogInterface;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.taketeacher.teacher.model.User;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class UbahPasswordActivity extends AppCompatActivity {
    EditText password_lama,password_baru,password_baru_ulang;
    Preference pref;
    String passwordOld;
    String passwordNew;
    String passwordConfirmation;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        progress = new ProgressDialog(this);

        password_lama = (EditText) findViewById(R.id.ubah_password_lama);
        password_baru = (EditText) findViewById(R.id.password_ubah_baru);
        password_baru_ulang = (EditText) findViewById(R.id.password_repeat_ubah);


        Button btn_login = (Button) findViewById(R.id.btn_action_ubah_password);
        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                passwordOld = password_lama.getText().toString();
                passwordNew = password_baru.getText().toString();
                passwordConfirmation = password_baru_ulang.getText().toString();
                gantiPassword();
                // Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    void gantiPassword(){

        pref = new Preference(getApplicationContext());
        String host  = pref.getHostName() + "user/changepassword?";
        // host += "&id=" + subKategoriId;
        host += "access-token=" + pref.getToken();
        // host += "&page=1";

        Map<String,String> params = new HashMap<String, String>();
        params.put("passwordOld",passwordOld);
        params.put("passwordNew",passwordNew);
        params.put("passwordRetype",passwordConfirmation);


        GsonRequest<User> jsObjRequest = new GsonRequest<User>(
                Request.Method.POST,
                host,
                User.class,
                params,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Loading");
        progress.setMessage("Ubah Password..");
        progress.setCancelable(false);
        progress.show();
    }

    private Response.Listener<User> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<User>() {
            @Override
            public void onResponse(User response) {

                progress.dismiss();
                Toast.makeText(getApplicationContext(),"Ubah Password Berhasil",Toast.LENGTH_LONG).show();
                finish();


            }
        };
    }

    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UbahPasswordActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    android.app.AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UbahPasswordActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    android.app.AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UbahPasswordActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    android.app.AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UbahPasswordActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    android.app.AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UbahPasswordActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    android.app.AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();

            }
        };
    }
}
