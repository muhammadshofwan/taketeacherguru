package com.taketeacher.teacher.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

import com.taketeacher.teacher.model.ViewTag;

/**
 * Created by DELL on 22/01/2018.
 */

public class AppHelper {

    /**
     * Turn drawable resource into byte array.
     *
     * @param context parent context
     * @param id      drawable resource id
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] getFileDataFromUri(Context context, Uri uri) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            parcelFileDescriptor.close();
            byte[] bbytes = byteArrayOutputStream.toByteArray();
            return  bbytes;
        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
    }

    public static byte[] getFileDataFromPdfUri(Context context, String path) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileInputStream fis;
        try {
            fis = new FileInputStream(new File(path));
            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = fis.read(buf)))
                baos.write(buf, 0, n);
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] bbytes = baos.toByteArray();
        return  bbytes;
    }

    public static byte[] getFileDataFromCamera(Context context, Bitmap bitmap) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] bbytes = byteArrayOutputStream.toByteArray();
            return  bbytes;
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    public  static boolean isValidate(Context context, ConstraintLayout layout){
        boolean result  = true;
        final int childCount = layout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ConstraintLayout container = (ConstraintLayout) layout.getChildAt(i);
            final int containerCount = container.getChildCount();
            for (int j = 0; j < containerCount; j++) {
                View element = container.getChildAt(j);
                if(element.getTag() != null){
                    String tagType =  element.getTag().getClass().getName();
                    if(tagType.equals("com.synergy.taketeacher.student.model.ViewTag")){
                        ViewTag viewTag = (ViewTag) element.getTag();
                        if(viewTag.getRequired() != 0){
                            // EditText
                            if (element instanceof Button) {
                                Button button = (Button)element;
                                if(!viewTag.getFilled()){
                                    button.setError("Wajib diisi.");
                                    result = false;
                                }else {
                                    button.setError(null);
                                }
                            }

                            // EditText
                            if (element instanceof EditText) {
                                EditText editText = (EditText)element;
                                if(editText.getText().toString().equals("")){
                                    editText.setError("Wajib diisi.");
                                    result = false;
                                }else{
                                    editText.setError(null);
                                }
                            }

                            // CheckBox
                            if (element instanceof CheckBox) {
                                CheckBox checkBox = (CheckBox)element;
                                if(!checkBox.isChecked()){
                                    checkBox.setError("Wajib diisi.");
                                    result = false;
                                }else {
                                    checkBox.setError(null);
                                }
                            }

                            // DatePicker
                            if (element instanceof DatePicker) {
                                DatePicker datePicker = (DatePicker)element;
                                if(datePicker.getDayOfMonth() == 0){
                                    result = false;
                                }
                            }

                            // Spinner
                            if (element instanceof Spinner) {
                                Spinner spinner = (Spinner)element;
                                if(spinner.getSelectedItemPosition() < 0){
                                    ((TextView)spinner.getChildAt(0)).setError("Wajib diisi.");
                                    result = false;
                                }else {
                                    ((TextView)spinner.getChildAt(0)).setError(null);
                                }
                            }
                        }else{
                            // EditText
                            if (element instanceof Button) {
                                Button button = (Button)element;
                                button.setError(null);
                            }

                            // EditText
                            if (element instanceof EditText) {
                                EditText editText = (EditText)element;
                                editText.setError(null);
                            }

                            // CheckBox
                            if (element instanceof CheckBox) {
                                CheckBox checkBox = (CheckBox)element;
                                checkBox.setError(null);
                            }


                            // Spinner
                            if (element instanceof Spinner) {
                                Spinner spinner = (Spinner)element;
                                ((TextView)spinner.getChildAt(0)).setError(null);
                            }
                        }
                    }
                }
            }
        }

        if(!result){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Perhatian");
            builder.setMessage("Silakan periksa kelengkapan data anda.")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return  result;
    }

}