package com.taketeacher.teacher.utils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anggara Jauhari on 23/08/2017.
 */

public class Meta {
    @SerializedName("totalCount") private int totalCount;
    @SerializedName("pageCount") private int pageCount;
    @SerializedName("currentPage") private int currentPage;
    @SerializedName("perPage") private int perPage;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }
}
