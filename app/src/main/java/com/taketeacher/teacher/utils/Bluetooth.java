package com.taketeacher.teacher.utils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;

public class Bluetooth {
private static final int REQCODE_BLUETOOTH_RESULT = 0;
	private String message;
	private Context context;
	private Activity activity;
	
	BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    
    OutputStream mmOutputStream;
    InputStream mmInputStream;

    Thread workerThread;
    
    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;
    
    public Bluetooth ( Context context, Activity activity){
		this.context= context;
		this.activity = activity;
	}
    
	public Bluetooth (String message, Context context, Activity activity){
		this.message = message;
		this.context= context;
		this.activity = activity;
	}
    
	public void print(){
		try {	
			if(!findBT()){
				//Toast toast = Toast.makeText( context, "Find BT", Toast.LENGTH_SHORT);
	            //toast.show();
			}
			if(!openBT()){
				//Toast toast = Toast.makeText( context, "Open BT", Toast.LENGTH_SHORT);
	            //toast.show();
			}
			
			if(!sendData()){
				//Toast toast = Toast.makeText( context, "Send Data", Toast.LENGTH_SHORT);
	            //toast.show();
			}
			if(!closeBT()){
				//Toast toast = Toast.makeText( context, "Close BT", Toast.LENGTH_SHORT);
	            //toast.show();
			}
		} catch (IOException e) {
			//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
			e.printStackTrace();
		}
	}
	
	// This will find a bluetooth printer device
    public boolean findBT() {
    	boolean result;
    	Preference pref = new Preference(context);
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
 
            if (mBluetoothAdapter == null) {
            	Toast toast = Toast.makeText(context, "Tidak adak bluetooth yang tersedia.", Toast.LENGTH_LONG);
				toast.show();
            }
 
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBluetooth, 0);
            }
 
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getAddress().equals(pref.getMacAddress())) {
                        mmDevice = device;
                        break;
                    }
                }
            }else{
            	//Toast toast = Toast.makeText( context, "Printer tidak ditemukan.", Toast.LENGTH_SHORT);
                //toast.show();
                result = false;
            }
            result= true;
        } catch (NullPointerException e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result = false;
            e.printStackTrace();
        } catch (Exception e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result = false;
            e.printStackTrace();
        }
        return result;
    }
      
    // Tries to open a connection to the bluetooth printer device
    public boolean openBT() throws IOException {
    	boolean result;
        try {
            // Standard SerialPortService ID
            //UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            //UUID uuid = mmDevice.getUuids()[0].getUuid();
            Method m = mmDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
            mmSocket = (BluetoothSocket) m.invoke(mmDevice, 1);
            
            //mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            if (mmSocket.isConnected()){
            	mmSocket.close();
            }
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
 
            beginListenForData();
            result=true;
        } catch (NullPointerException e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result=false;
            e.printStackTrace();
        } catch (Exception e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result=false;
            e.printStackTrace();
        }
        return result;
    }

    public boolean beginListenForData() {
    	boolean result;
        try {
            final Handler handler = new Handler();
             
            final byte delimiter = 10;
 
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];
           
            workerThread = new Thread(new Runnable() {            	
                public void run() {            	
                    
                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {                         
                        try {                             
                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0; 
                                        handler.post(new Runnable() {
                                            public void run() {   
                                            	
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }                             
                        } catch (IOException ex) {
                            stopWorker = true;
                        }                         
                    }
                }
            });
 
            workerThread.start();
            result=true;
        } catch (NullPointerException e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result=false;
            e.printStackTrace();
        } catch (Exception e) {
        	Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();
        	result=false;
            e.printStackTrace();
        }
        return result;
    }

    public boolean sendData() throws IOException {
    	boolean result;
        try {              	
            mmOutputStream.write(message.getBytes()); 
            result =true;          
        } catch (NullPointerException e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result= false;
            e.printStackTrace();
        } catch (Exception e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result= false;
            e.printStackTrace();
        }
        
        return result;
    }
 
    // Close the connection to bluetooth printer.
    public boolean closeBT() throws IOException {
    	boolean result;
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            result = true;
        } catch (NullPointerException e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result = false;
            e.printStackTrace();
        } catch (Exception e) {
        	//Toast toast = Toast.makeText( context, e.getMessage(), Toast.LENGTH_SHORT);
            //toast.show();
        	result = false;
            e.printStackTrace();
        }
        return result;
    }
    
    
}
