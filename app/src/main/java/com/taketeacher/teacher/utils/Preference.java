package com.taketeacher.teacher.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preference {
	
	public Preference(Context context){
		this.context = context;
	};
	
	private Context context;
	
	public String Read(String key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.context);
        return pref.getString(key, "");
    }
 
    public void Write(String key, final String value) {
          SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.context);
          SharedPreferences.Editor editor = settings.edit();
          editor.putString(key, value);
          editor.commit();        
    }
     
    // Boolean  
    public  boolean ReadBoolean(String key,boolean defaultValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.context);
        return settings.getBoolean(key, defaultValue);
    }
 
    public void WriteBoolean(String key,boolean value) {
          SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.context);
          SharedPreferences.Editor editor = settings.edit();
          editor.putBoolean(key, value);
          editor.commit();        
    }

    public String getNama() {
        return this.Read("nama");
    }
    public void setNama(String nama) {
        this.Write("nama", nama);
    }
	public String getToken() {
		return this.Read("token");
	}
	public void setToken(String token) {
		this.Write("token", token);
	}
	public String getUserId() {
		return this.Read("user_id");
	}
	public void setUserId(String userId) {
		this.Write("user_id", userId);
	}
	public String getHostName() {
		return this.Read("HostName");
	}	
	public void setHostName(String HostName) {
		this.Write("HostName", HostName);
	}
	public String getMacAddress() {
		return this.Read("MacAddress");
	}	
	public void setMacAddress(String MacAddress) {
		this.Write("MacAddress", MacAddress);
	}
	public String getUserName() {
		return this.Read("UserName");
	}
	public void setUserName(String UserName) {
		this.Write("UserName", UserName);
	}
	public String getPassword() {
		return this.Read("Password");
	}
	public void setPassword(String Password) {
		this.Write("Password", Password);
	}
	public String getKode() {
		return this.Read("Kode");
	}
	public void setKode(String Kode) {
		this.Write("Kode", Kode);
	}
    public String getDeleteCode() {
        return this.Read("DeleteCode");
    }
    public void setDeleteCode(String DeleteCode) {
        this.Write("DeleteCode", DeleteCode);
    }
    public String geEmailAuthHash() {
		return this.Read("emailAuthHash");
	}
	public void setEmailAuthHash(String emailAuthHash) {
		this.Write("emailAuthHash", emailAuthHash);
	}


}
