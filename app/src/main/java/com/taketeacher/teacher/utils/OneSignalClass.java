package com.taketeacher.teacher.utils;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import com.taketeacher.teacher.DetailJadwalActivity;
import com.taketeacher.teacher.ListRatingSayaActivity;
import com.taketeacher.teacher.MainActivity;


public class OneSignalClass extends Application {
    private static Context context;

    public static Context getContext() {
        return context;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .init();
    }
    private class MyNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
            JSONObject data = notification.payload.additionalData;
            String notificationID = notification.payload.notificationID;
            String title = notification.payload.title;
            String body = notification.payload.body;
            String smallIcon = notification.payload.smallIcon;
            String largeIcon = notification.payload.largeIcon;
            String bigPicture = notification.payload.bigPicture;
            String smallIconAccentColor = notification.payload.smallIconAccentColor;
            String sound = notification.payload.sound;
            String ledColor = notification.payload.ledColor;
            int lockScreenVisibility = notification.payload.lockScreenVisibility;
            String groupKey = notification.payload.groupKey;
            String groupMessage = notification.payload.groupMessage;
            String fromProjectNumber = notification.payload.fromProjectNumber;
            String rawPayload = notification.payload.rawPayload;

            String customKey;

            Log.i("OneSignalExample", "NotificationID received: " + notificationID);

            if (data != null) {
                customKey = data.optString("customkey", null);
                if (customKey != null)
                    Log.i("OneSignalExample", "customkey set with value: " + customKey);
            }
        }
    }


    private class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
       // private Application application;

       // public ExampleNotificationOpenedHandler() {
         //   this.application = application;
        //}
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;
            // Object activityToLaunch;
            // Object activityToLaunch = null;
            try {
                if (data.get("module").equals("deposit") ){
                    //  activityToLaunch = ListDepositMutasiActivity.class;
                    int page = 2;
                    Intent intent = new Intent(context,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("page", page);
                    startActivity(intent);

                }else if (data.get("module").equals("jadwal")){
                    Intent intent = new Intent(context,DetailJadwalActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", (Integer) data.get("id"));
                    intent.putExtra("idGuru", data.get("id_murid").toString());
                    startActivity(intent);

                }else if (data.get("module").equals("rating")){
                    Intent intent = new Intent(context,ListRatingSayaActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    // intent.putExtra("id", data.get("id").toString());
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(context,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String launchUrl = result.notification.payload.launchURL; // update docs launchUrl
            // String openURL = null;
            String customKey;
            // Intent intent = n
            // ew Intent(context,NotifActivity.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //Intent.FLAG_ACTIVITY_REORDER_TO_FRONT |
            // context.startActivity(intent);


            if (data != null) {
                customKey = data.optString("customkey", null);
                launchUrl = data.optString("openURL", null);

                if (customKey != null)
                    Log.i("OneSignalExample", "customkey set with value: " + customKey);

                if (launchUrl != null)
                    Log.i("OneSignalExample", "openURL to webview with URL value: " + launchUrl);
            }

            if (actionType == OSNotificationAction.ActionType.ActionTaken) {
                Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);

                if (result.action.actionID.equals("id1")) {
                    Log.i("OneSignalExample", "button id called: " + result.action.actionID);
                    //  activityToLaunch = RatingActivity.class;
                } else
                    Log.i("OneSignalExample", "button id called: " + result.action.actionID);
            }
            // The following can be used to open an Activity of your choice.
            // Replace - getApplicationContext() - with any Android Context.
            // Intent intent = new Intent(getApplicationContext(), YourActivity.class);
            //  Intent i = new Intent(getApplicationContext(), (Class<?>) activityToLaunch);
            // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            //  i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);

            //  i.putExtra("asd", data);
            // i.putExtra("push_message", data);
            // i.putExtra("openURL", data);
            //  Log.i("OneSignalExample", "openURL = " + openURL);
            // startActivity(intent);
            // startActivity(i);

        }
    }

}
