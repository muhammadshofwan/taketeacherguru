package com.taketeacher.teacher.utils;

import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class JSON {
	public static JSONObject GetData(String data){
		JSONObject result = new JSONObject();		
		try {		
	        JSONObject json = new JSONObject(data);
	        result = json;        
		}catch (Exception e) {

		}
		return result;		
	}
	
	public static ArrayList<HashMap<String, String>> GetDataTable(JSONObject data){
			ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
	        try {   
				int success = data.getInt("success");
				if (success == 1) {
					JSONArray hasil = data.getJSONArray("result");    				
					for (int i = 0; i < hasil.length(); i++) {
						JSONObject c = hasil.getJSONObject(i);
						Iterator keys = c.keys();
						HashMap<String, String> map = new HashMap<String, String>();
						while(keys.hasNext()) {
					        String JSONkey = (String)keys.next();
					        String JSONValue = c.getString(JSONkey);
					        map.put(JSONkey, JSONValue);
					    }
						result.add(map);   					
					}
				} else {
					
				}
			} catch (JSONException e) {

			}
	        return result;
	}
	
	public static ArrayList<HashMap<String, String>> GetDataTable(JSONObject data, String tag){
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        try {   
				JSONArray hasil = data.getJSONArray(tag);    				
				for (int i = 0; i < hasil.length(); i++) {
					JSONObject c = hasil.getJSONObject(i);
					Iterator keys = c.keys();
					HashMap<String, String> map = new HashMap<String, String>();
					while(keys.hasNext()) {
				        String JSONkey = (String)keys.next();
				        String JSONValue = c.getString(JSONkey);
				        map.put(JSONkey, JSONValue);
				    }
					result.add(map);   					
				}
		} catch (JSONException e) {

		}
        return result;
}
	
	public static ArrayList<HashMap<String, String>> GetDataTable(JSONArray data){
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        try {     				
				for (int i = 0; i < data.length(); i++) {
					JSONObject c = data.getJSONObject(i);
					Iterator keys = c.keys();
					HashMap<String, String> map = new HashMap<String, String>();
					while(keys.hasNext()) {
				        String JSONkey = (String)keys.next();
				        String JSONValue = c.getString(JSONkey);
				        map.put(JSONkey, JSONValue);
				    }
					result.add(map);   					
				}
		} catch (JSONException e) {

		}
        return result;
}

	public static int JSONSTATUS(JSONObject data){
		int result = 0;
        try {   
        	result = data.getInt("success");
		} catch (JSONException e) {

		}
        return result;
	}

	public static void setObjectJSON(Object obj,JSONObject json) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		try {   
			Iterator keys = json.keys();
			Class  aClass = obj.getClass();
			while(keys.hasNext()) {
		        String JSONkey = (String)keys.next();
		        Object JSONValue = json.getString(JSONkey);
		        Field f = aClass.getDeclaredField(JSONkey);
		        f.set(obj, JSONValue);
		    }
		} catch (JSONException e) {

		}
	}

	public static JSONObject GetObjectJSON(Object obj) throws IllegalArgumentException, IllegalAccessException, JSONException{
		JSONObject result = new JSONObject();
		Class  aClass = obj.getClass();
        Field[] fields = aClass.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            if(f.get(obj) != null){
            	result.put(f.getName(), (String) f.get(obj));
            }            
        } 
		return null;		
	}

    public static void JSONSplit(JSONObject rowObject,Cursor cursor ){
        for( int i=0 ;  i< cursor.getColumnCount() ; i++ ){
            if( cursor.getColumnName(i) != null ){
                try{
                    if( cursor.getString(i) != null ){
                        rowObject.put(cursor.getColumnName(i) ,  cursor.getString(i) );
                    }else{
                        rowObject.put( cursor.getColumnName(i) ,  "" );
                    }
                }catch( Exception e ){
                    e.printStackTrace();
                }
            }
        }
    }
}
