package com.taketeacher.teacher.utils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anggara Jauhari on 23/08/2017.
 */

public class Link {
    @SerializedName("self") private Link self;
    @SerializedName("first") private Link first;
    @SerializedName("prev") private Link prev;
    @SerializedName("next") private Link next;
    @SerializedName("last") private Link last;
    @SerializedName("href") private String href;

    public Link getSelf() {
        return self;
    }

    public void setSelf(Link self) {
        this.self = self;
    }

    public Link getFirst() {
        return first;
    }

    public void setFirst(Link first) {
        this.first = first;
    }

    public Link getPrev() {
        return prev;
    }

    public void setPrev(Link prev) {
        this.prev = prev;
    }

    public Link getNext() {
        return next;
    }

    public void setNext(Link next) {
        this.next = next;
    }

    public Link getLast() {
        return last;
    }

    public void setLast(Link last) {
        this.last = last;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
