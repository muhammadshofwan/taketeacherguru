package com.taketeacher.teacher;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.onesignal.OneSignal;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.taketeacher.teacher.model.User;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class LoginActivity extends AppCompatActivity {

    private EditText txt_password, txt_username;
    private Button btn_login;
    private CheckBox mCbShowPwd;
    ProgressDialog progress ;
    private TextView btn_link_daftar, txt_lupa_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txt_username = (EditText) findViewById(R.id.txt_nama);
        txt_password = (EditText) findViewById(R.id.txt_password);
        // get the show/hide password Checkbox
        mCbShowPwd = (CheckBox) findViewById(R.id.cbShowPwd);

        // add onCheckedListener on checkbox
        // when user clicks on this checkbox, this is the handler.
        mCbShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // show password
                    txt_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    txt_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        txt_lupa_password = (TextView) findViewById(R.id.lupa_password);
        txt_lupa_password.setPaintFlags(txt_lupa_password.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        txt_lupa_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LupaPasswordActivity.class);
               startActivity(i);
            }
        });


        // Set up the login form.

        txt_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    login();
                    return true;
                }
                return false;
            }
        });


        progress = new ProgressDialog(this);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                login();

            }
        });

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_SMS,
                Manifest.permission.CAMERA};

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }



    public void login(){
        Preference pref = new Preference(getApplicationContext());
        String username = txt_username.getText().toString();
        String password = txt_password.getText().toString();
        String host = pref.getHostName() + "auth/login";

        Map<String,String> params = new HashMap<String, String>();
        params.put("username",username);
        params.put("password",password);

        GsonRequest<User> jsObjRequest = new GsonRequest<User>(
                Request.Method.POST,
                host,
                User.class,
                params,
                this.loginReqSuccessListener(),
                this.loginReqErrorListener());

        // Adding request to request queue
        AppController controller = new AppController(getApplication());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Loading Login");
        progress.setMessage("Loading Login...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<User> loginReqSuccessListener() {
        return new Response.Listener<User>() {
            @Override
            public void onResponse(User response) {
                // Do whatever you want to do with response;
                // Like response.tags.getListing_count(); etc. etc.
                //pDialog.hide();
                progress.dismiss();
                Preference pref = new Preference(getApplicationContext());
                pref.setToken(response.getToken());
                pref.setEmailAuthHash(response.getEmailAuthHash());
                Intent mainIntent =  new Intent(getApplicationContext(),MainActivity.class);
                String email = response.getEmail();
                String emailAuthHash = response.getEmailAuthHash(); // Email auth hash generated from your server
                OneSignal.setEmail(email, emailAuthHash, new OneSignal.EmailUpdateHandler() {
                    @Override
                    public void onSuccess() {
                        // Email successfully synced with OneSignal
                    }

                    @Override
                    public void onFailure(OneSignal.EmailUpdateError error) {
                        // Error syncing email, check error.getType() and error.getMessage() for details
                    }
                });
                startActivity(mainIntent);
                finish();
                Toast.makeText(getApplicationContext(),
                        "Login Berhasil",
                        Toast.LENGTH_LONG).show();
            }
        };

    }
    private Response.ErrorListener loginReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                  //  finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                 //   finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                  //  finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                  //  finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                   // finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();

            }
        };
    }

    /**private void attemptLogin() {

     // Store values at the time of the login attempt.
     String username = txt_username.getText().toString();
     String password = txt_password.getText().toString();

     Intent intensLoading = new Intent(getApplicationContext(),LoadingLoginActivity.class);
     intensLoading.putExtra("username", username);
     intensLoading.putExtra("password", password);
     startActivity(intensLoading);
     finish();
     } **/
}
