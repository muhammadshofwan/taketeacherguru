package com.taketeacher.teacher;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.io.UnsupportedEncodingException;

import com.taketeacher.teacher.adapter.DepositMutasiAdapter;
import com.taketeacher.teacher.model.DepositMutasi;
import com.taketeacher.teacher.model.Guru;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.FormatText;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;


/**
 * A simple {@link Fragment} subclass.
 */
public class
FragmentDeposit extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ShimmerFrameLayout mShimmerViewContainer1;
    private ShimmerFrameLayout mShimmerViewContainer2;
    private RelativeLayout view_summary;
    private TextView txt_saldo;
    private LinearLayout layout_kosong, layout_koneksi;
    // ArrayList<Deposit> data = new ArrayList<Deposit>();
    private RecyclerView rv_deposit;
    Preference pref;
    Context context;
    private Button btn_refresh;
    private SwipeRefreshLayout swipeRefreshLayout;
    public FragmentDeposit() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_deposit, container, false);
        view_summary = (RelativeLayout) v.findViewById(R.id.view_summary);
        view_summary.setVisibility(View.GONE);
        mShimmerViewContainer1 = v.findViewById(R.id.shimmer_view_container1);
        mShimmerViewContainer2 = v.findViewById(R.id.shimmer_view_container2);
        this.context = v.getContext();

        layout_koneksi = (LinearLayout) v.findViewById(R.id.layout_koneksi);

        btn_refresh = (Button) v.findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(FragmentDeposit.this).attach(FragmentDeposit.this).commit();
            }
        });
        txt_saldo = (TextView) v.findViewById(R.id.txt_saldo_teacher_pay);

        rv_deposit =(RecyclerView) v.findViewById(R.id.rv_deposit_mutasi);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rv_deposit.setLayoutManager(mLayoutManager);
        rv_deposit.setHasFixedSize(true);

        rv_deposit.setVisibility(View.GONE);
       // ImageView btn_bayar = (ImageView) v.findViewById(R.id.tambah);
       // btn_bayar.setOnClickListener(new View.OnClickListener() {

          //  @Override
           // public void onClick(View v) {
         //       Intent intent = new Intent(getActivity().getApplicationContext(), TambahDepositActivity.class);
           //     startActivity(intent);
        //    }
        //});
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);


        return v;
    }

    void getSaldoSaya(){
        pref = new Preference(getActivity().getApplicationContext());
        String host  = pref.getHostName() + "user/profile?";
        // host += "&id=" + idSubKategori;
        host += "access-token=" + pref.getToken();
        // host += "&page=1";
        GsonRequest<Guru> jsObjRequest = new GsonRequest<Guru>(
                Request.Method.GET,
                host,
                Guru.class,
                this.downloadDataPelajaranReqSuccessListener1(),
                this.downloadDataPelajaranReqErrorListener1());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getActivity().getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);
    }
    private Response.Listener<Guru> downloadDataPelajaranReqSuccessListener1() {
        return new Response.Listener<Guru>() {
            @Override
            public void onResponse(Guru response) {

                Guru profil = response;

                txt_saldo.setText(FormatText.rupiahFormat(profil.getSaldo()));
                //  nama_belakang_txt.setText(profil.get());
                // jumlah.setText("Rp. " + Integer.toString(profil.getJumlah()));


            }
        };
    }
    private Response.ErrorListener downloadDataPelajaranReqErrorListener1() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
            }
        };
    }


    void downloadDataDepositMutasi(){
        pref = new Preference(getActivity().getApplicationContext());
        String host  = pref.getHostName() + "depositomutasi?";
        // host += "&id=" + idSubKategori;
        //host += "DepositoMutasiSearch[user_id]=" + pref.getUserId();
        host += "&sort=-tanggal";
        host += "&access-token=" + pref.getToken();
        GsonRequest<DepositMutasi> jsObjRequest = new GsonRequest<DepositMutasi>(
                Request.Method.GET,
                host,
                DepositMutasi.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getActivity().getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        mShimmerViewContainer1.startShimmerAnimation();
        mShimmerViewContainer1.setVisibility(View.VISIBLE);

        mShimmerViewContainer2.startShimmerAnimation();
        mShimmerViewContainer2.setVisibility(View.VISIBLE);

        layout_koneksi.setVisibility(View.GONE);

    }
    private Response.Listener<DepositMutasi> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<DepositMutasi>() {
            @Override
            public void onResponse(DepositMutasi response) {
                mShimmerViewContainer1.stopShimmerAnimation();
                mShimmerViewContainer1.setVisibility(View.GONE);

                mShimmerViewContainer2.stopShimmerAnimation();
                mShimmerViewContainer2.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                DepositMutasiAdapter adapter = new DepositMutasiAdapter(getActivity(),response.getDepositMutasiList());
                rv_deposit.setAdapter(adapter);
                rv_deposit.setVisibility(View.VISIBLE);
                view_summary.setVisibility(View.VISIBLE);
            }
        };
    }
    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    layout_koneksi.setVisibility(View.VISIBLE);
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();

                mShimmerViewContainer1.stopShimmerAnimation();
                mShimmerViewContainer1.setVisibility(View.GONE);

                mShimmerViewContainer2.stopShimmerAnimation();
                mShimmerViewContainer2.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);

            }
        };
    }


    @Override
    public void onResume() {
        super.onResume();
        view_summary.setVisibility(View.GONE);
        rv_deposit.setVisibility(View.GONE);
        getSaldoSaya();
        downloadDataDepositMutasi();


    }

    @Override
    public void onPause() {
        mShimmerViewContainer1.stopShimmerAnimation();
        mShimmerViewContainer1.setVisibility(View.GONE);

        mShimmerViewContainer2.stopShimmerAnimation();
        mShimmerViewContainer2.setVisibility(View.GONE);
        super.onPause();
    }

    @Override
    public void onRefresh() {
        view_summary.setVisibility(View.GONE);
        rv_deposit.setVisibility(View.GONE);
        getSaldoSaya();
        downloadDataDepositMutasi();
    }
}
