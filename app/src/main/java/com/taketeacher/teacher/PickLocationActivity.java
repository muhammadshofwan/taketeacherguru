package com.taketeacher.teacher;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.taketeacher.teacher.model.Guru;

public class PickLocationActivity extends AppCompatActivity {

    TextView txtLocationAddress;
    SupportMapFragment mapFragment;
    GoogleMap map;
    LatLng center;
    CardView cardView;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final String TAG = "PickLocationActivity";
    String label_alamat;
    private double longitude;
    private double latitude;
    Guru profils;
    ProgressDialog progress;
    private FusedLocationProviderClient mFusedLocationClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_location);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            profils = (Guru) extras.getSerializable("profil");
            if(profils  != null){
                //txtLocationAddress.setText(profils.getAlamat());
                latitude = Double.parseDouble(profils.getLat());
                longitude = Double.parseDouble(profils.getLng());
            }else {

            }


        }else {

        }
        progress = new ProgressDialog(this);
        txtLocationAddress = findViewById(R.id.txtLocationAddress);
        //  txtLocationAddress.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        // txtLocationAddress.setSingleLine(true);
        // txtLocationAddress.setMarqueeRepeatLimit(-1);
        txtLocationAddress.setSelected(true);

        cardView = findViewById(R.id.cardView);
        final AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(Place.TYPE_COUNTRY)
                .setCountry("ID")
                .build();
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  Intent intent =
                //        null;
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setFilter(autocompleteFilter)
                                    .build(PickLocationActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    center = new LatLng(longitude, latitude);
                    if ( longitude == 0 ||  latitude == 0) {

                        center = new LatLng(location.getLatitude(),location.getLongitude());
                        //  map.addMarker(new MarkerOptions().position(center));
                        moveMap(center);
                    }
                }
            }
        });
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            //  @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                //  map.setOnMapClickListener((GoogleMap.OnMapClickListener) getApplicationContext());
                map.getUiSettings().setZoomControlsEnabled(true);
                // map.setMyLocationEnabled(this);
                center = new LatLng(longitude, latitude);
                if ( longitude == 0 ||  latitude == 0) {
                    enableMyLocation();
                }
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 16));
                initCameraIdle();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pick, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        if (id == R.id.action_simpan) {
            label_alamat = txtLocationAddress.getText().toString();
            latitude = center.longitude;
            longitude = center.latitude;

            Intent returnIntent = new Intent();
            //returnIntent.putExtra("profil",profils);
            returnIntent.putExtra("alamat",label_alamat);
            returnIntent.putExtra("latitude",latitude);
            returnIntent.putExtra("longitude",longitude);
            setResult(RESULT_OK,returnIntent);
            finish();

            //postAlamat();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initCameraIdle() {
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                // if (longitude == 0 || latitude == 0) {
                //  enableMyLocation();
                //   }else {
                center = map.getCameraPosition().target;
                getAddressFromLocation(center.latitude, center.longitude);
                // }
            }
        });

    }
    private void getAddressFromLocation(double longitude, double latitude) {

        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);


        try {
            List<Address> addresses = geocoder.getFromLocation(longitude, latitude , 1);

            if (addresses.size() > 0) {
                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();
                for (int i = 0; i < fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append("");
                }
                txtLocationAddress.setText(fetchedAddress.getAddressLine(0).toString());

            } else {
                txtLocationAddress.setText("Searching Current Address");
            }

        } catch (IOException e) {
            e.printStackTrace();
            printToast("Could not get address..!");
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(PickLocationActivity.this, data);
                if (!place.getAddress().toString().contains(place.getName())) {
                    txtLocationAddress.setText(place.getName() + ", " + place.getAddress());
                } else {
                    txtLocationAddress.setText(place.getAddress());
                }

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16);
                map.animateCamera(cameraUpdate);


            }  else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
    private void printToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        } else if (map != null) {
            // Access to the location has been granted to the app.
            map.setMyLocationEnabled(true);

            //center = new LatLng(location.getLatitude(), location.getLongitude());

            // myLocation = map.getCameraPosition().target;
            //  getAddressFromLocation(center.latitude, center.longitude);
            //  mMap.addMarker(new MarkerOptions().position(myLocation));
        }
    }

    private void moveMap(LatLng latLng) {
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(16));
        map.getUiSettings().setZoomControlsEnabled(true);
    }
}
