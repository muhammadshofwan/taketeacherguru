package com.taketeacher.teacher;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.taketeacher.teacher.model.Guru;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.AppHelper;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;
import com.taketeacher.teacher.utils.VolleyMultipartRequest;
import com.taketeacher.teacher.utils.VolleySingleton;

public class InputProfileActivity extends AppCompatActivity  {

    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private GoogleApiClient googleApiClient;
    private LatLng myLocation;
    private ImageButton btn_tambah_alamat;
    private FusedLocationProviderClient mFusedLocationClient;
    EditText txt_nama_depan, txt_nama_belakang_, txt_alamat, txt_tgl_lahir, txt_tpt_lahir, txt_telepon;
    DatePickerDialog datePickerDialog;
    Preference pref;
    FloatingActionButton fab;
    private ImageView ava_murid;
    Guru profils;
    ArrayList<String> jenisKelamin;
    Spinner jenis_kelamin;
    String nama_depan;
    String nama_belakang;
    String telpon;
    String alamat;
    String tempat_lahir;
    String tanggal_lahir;
    String gender;
    String agama;
    String host;
    Spinner agama_txt;
    ArrayList<String> metode_pembayaran = new ArrayList<String>();
    public static final int PICK_IMAGE = 1;
    //String jenis_kelamin;

    int id;

    ProgressDialog progress;
    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.
    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_profile);

        if (checkPermissions()){
            //  permissions  granted.
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            //id = extras.getInt("profil");
            if (extras.getString("source").equals("register")){

            }else {

                profils = (Guru) extras.getSerializable("profil");
            }
        }
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);





        progress = new ProgressDialog(this);
        btn_tambah_alamat = (ImageButton) findViewById(R.id.btn_alamat_tambah);
        btn_tambah_alamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PickLocationActivity.class);
                // intent.putExtra("longitude",longitude);
                // intent.putExtra("latitude",latitude);
                // intent.putExtra("alamat",alamat);
                intent.putExtra("profil",profils);
                startActivityForResult(intent,101);
            }
        });
        ava_murid = (ImageView) findViewById(R.id.ava_guru);
        // uri= FilePicker.getFileUri(this, resultCode, data);
        ava_murid.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                com.github.dhaval2404.imagepicker.ImagePicker.Companion.with(InputProfileActivity.this)
                        .crop(1f,1f) //Crop image(Optional), Check Customization for more option
                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(512, 512)	//Final image resolution will be less than 1080 x 1080(Optional)
                        .start(PICK_IMAGE);

            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // Intent chooseImageIntent = ImagePicker.getPickImageIntent(getApplicationContext());
               // startActivityForResult(chooseImageIntent, PICK_IMAGE);
             //   CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON).start(InputProfileActivity.this);

                com.github.dhaval2404.imagepicker.ImagePicker.Companion.with(InputProfileActivity.this)
                        .crop(1f,1f) //Crop image(Optional), Check Customization for more option
                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(512, 512)	//Final image resolution will be less than 1080 x 1080(Optional)
                        .start(PICK_IMAGE);

            }
        });


        txt_nama_depan = (EditText) findViewById(R.id.input_nama_depan);
        txt_nama_belakang_ = (EditText) findViewById(R.id.input_nama_belakang);
        txt_alamat = (EditText) findViewById(R.id.input_alamat);
        txt_tgl_lahir = (EditText) findViewById(R.id.input_tanggal_lahir);
        txt_tgl_lahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(InputProfileActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String strYear, strMonth, strDay;
                                // set day of month , month and year value in the edit text
                                strYear = Integer.toString(year);
                                if(monthOfYear < 9){
                                    strMonth = "0" + Integer.toString(monthOfYear+1);
                                }else{
                                    strMonth = Integer.toString(monthOfYear+1);
                                }

                                if(dayOfMonth < 10){
                                    strDay = "0" + Integer.toString(dayOfMonth);
                                }else{
                                    strDay = Integer.toString(dayOfMonth);
                                }

                                txt_tgl_lahir.setText(strYear + "-" + strMonth + "-" + strDay);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        txt_tpt_lahir = (EditText) findViewById(R.id.input_tempat_lahir);
        txt_telepon = (EditText) findViewById(R.id.input_telepon);
       // txt_email = (EditText) findViewById(R.id.input_email_ubah);
        // = (EditText) findViewById(R.id.input_tempat_lahir);
        // jenisKelamin = new ArrayList<String>();
        jenis_kelamin = (Spinner) findViewById(R.id.spinner_profile);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, jeniskelamin);
        jenis_kelamin.setPrompt("Jenis Kelamin");
        jenis_kelamin.setAdapter(adapter);

        agama_txt = (Spinner) findViewById(R.id.spinner_profile_agama);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, agamaa);
        agama_txt.setPrompt("-Agama-");
        agama_txt.setAdapter(adapter1);
        /**
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_profil);
        mapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    if ( longitude == 0 ||  latitude == 0) {

                        myLocation = new LatLng(location.getLatitude(),location.getLongitude());
                        mMap.addMarker(new MarkerOptions().position(myLocation));
                        moveMap(myLocation);
                    }
                }
            }
        });

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build(); **/



        Button btn_login = (Button) findViewById(R.id.btn_simpan_profile);
        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                nama_depan = txt_nama_depan.getText().toString();
                nama_belakang = txt_nama_belakang_.getText().toString();
                telpon = txt_telepon.getText().toString();
                alamat = txt_alamat.getText().toString();
                tempat_lahir = txt_tpt_lahir.getText().toString();
                tanggal_lahir = txt_tgl_lahir.getText().toString();
                gender = Integer.toString(jenis_kelamin.getSelectedItemPosition());
                agama = Integer.toString(agama_txt.getSelectedItemPosition());
            //    longitude = profils.getLng()
            //    latitude
                // tanggal_lahir = txt_tgl_lahir.getText().toString();
//                email = txt_email.getText().toString();

                if (!validasi()) {

                    return;
                }else  if (gender.equals("0")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                    builder.setTitle("Perhatian");
                    builder.setMessage("Jenis kelamin harap dipilih!")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else if (agama.equals("0")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                    builder.setTitle("Perhatian");
                    builder.setMessage("Agama harap dipilih!")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else{

                    getDataProfil();
                }
                // nama_belakang = txt_nama_belakang_.getText().toString();
                //telpon = txt_telepon.getText().toString();
                //  Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                // startActivity(intent);
            }
        });


        if (extras != null){
            //id = extras.getInt("profil");
            if (extras.getString("source").equals("register")){

            }else {

                profils = (Guru) extras.getSerializable("profil");
                Picasso.with(getApplicationContext())
                        .load(profils.getImage_url())
                        .into(ava_murid);
                txt_nama_depan.setText(profils.getNama_depan());
                txt_nama_belakang_.setText(profils.getNama_belakang());
                txt_alamat.setText(profils.getAlamat());
                txt_tgl_lahir.setText(profils.getTanggal_lahir());
                txt_tpt_lahir.setText(profils.getTempat_lahir());
                txt_telepon.setText(profils.getTelpon());
                jenis_kelamin.setSelection(profils.getJenis_kelamin());
                agama_txt.setSelection(profils.getAgama());
                latitude = Double.parseDouble(profils.getLat());
                longitude = Double.parseDouble(profils.getLng());
            }
        }

    }
    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(getApplicationContext(),p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private static final String[] jeniskelamin = new String[] {
            "","Pria", "Wanita"
    };

    private static final String[] agamaa = new String[] {
            "","Islam", "Kristen Protestan", "Kristen Katholik", "Hindu", "Buddha"
    };
/**
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setOnMapClickListener(this);
       // enableMyLocation();
        mMap.clear();
        LatLng latLng = new LatLng(longitude,latitude);
        if ( longitude == 0 ||  latitude == 0) {
            //  myLocation = new LatLng(location.getLatitude(),location.getLongitude());
            // mMap.addMarker(new MarkerOptions().position(myLocation));
            //moveMap(myLocation);
            enableMyLocation();
        } else{
            MarkerOptions marker = new MarkerOptions().position(latLng);
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mMap.addMarker(marker);
            moveMap(latLng);
            longitude = latLng.latitude;
            latitude = latLng.longitude;
        }

    } **/


    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    /**
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);

            //LatLng myLocation = new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude());
            //mMap.addMarker(new MarkerOptions().position(myLocation));
        }
    }

    private void moveMap(LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        mMap.getUiSettings().setZoomControlsEnabled(true);
    } **/


/**
    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }
 **/
@Override
public void onActivityResult(int requestCode, int resultCode, Intent data)
{
    if (requestCode==101){
        if(resultCode==RESULT_OK){
            alamat = data.getStringExtra("alamat");
            longitude  = data.getDoubleExtra("longitude",0);
            latitude  = data.getDoubleExtra("latitude",0);

            txt_alamat.setText(alamat);
        }
    }else if (requestCode == PICK_IMAGE) {
        if (resultCode == RESULT_OK) {
            // Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);


            Uri fileUri = data.getData();
            File file = new File(fileUri.getPath());
            // bitmap = null;
            try {
                Bitmap   bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
                uploadCamera(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (resultCode == ImagePicker.RESULT_ERROR) {

        } else {
        }
    }

}



    public void uploadCamera(final Bitmap bitmap)
    {
        pref = new Preference(getApplicationContext());
        host  = pref.getHostName() + "user/changeimage?";
        // host += "&id=" + subKategoriId;
        host += "access-token=" + pref.getToken();

        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, host, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    String imageUrl = result.getString("image_url");
                    // String message = result.getString("message");
                    Picasso.with(getApplicationContext())
                            .load(imageUrl)
                            .into(ava_murid);
                    progress.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                // currentButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.avatar));
                Log.i("Error", errorMessage);
                error.printStackTrace();
                progress.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                builder.setTitle("Perhatian");
                builder.setMessage("Gagal update foto, silahkan cek kembali data atau jaringan anda")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                params.put("Guru[image]", new DataPart("image.jpg", AppHelper.getFileDataFromCamera(getBaseContext(), bitmap), "image/jpeg"));
                return params;
            }
/**
 @Override
 public Map<String, String> getHeaders() throws AuthFailureError {
 Map<String, String> headers = new HashMap<>();
 headers.put("Authorization", "Bearer " + pref.getToken());
 return headers != null ? headers : super.getHeaders();
 } **/
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        progress.setTitle("Uploading");
        progress.setMessage("Uploading data...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }


    /**
     private Bitmap getBitmapFromUri(Uri uri) throws IOException {
     ParcelFileDescriptor parcelFileDescriptor =
     getContentResolver().openFileDescriptor(uri, "r");
     FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
     Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
     parcelFileDescriptor.close();
     return image;
     } **/

    void getDataProfil(){

        pref = new Preference(getApplicationContext());
        host  = pref.getHostName() + "user/changeprofile?";
        // host += "&id=" + subKategoriId;
        host += "access-token=" + pref.getToken();

        Map<String,String> params = new HashMap<String, String>();
        params.put("Guru[nama_depan]",nama_depan);
        params.put("Guru[nama_belakang]",nama_belakang);
        params.put("Guru[telpon]",telpon);
        params.put("Guru[alamat]",alamat);
        params.put("Guru[tempat_lahir]",tempat_lahir);
        params.put("Guru[tanggal_lahir]", tanggal_lahir);
        params.put("Guru[jenis_kelamin]", gender);
        params.put("Guru[agama]", agama);
        params.put("Guru[lat]",Double.toString(latitude));
        params.put("Guru[lng]", Double.toString(longitude));

        GsonRequest<Guru> jsObjRequest = new GsonRequest<Guru>(
                Request.Method.POST,
                host,
                Guru.class,
                params,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Loading");
        progress.setMessage("Profile Loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }

    private Response.Listener<Guru> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<Guru>() {
            @Override
            public void onResponse(Guru response) {

                progress.dismiss();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                /**
                 AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                 builder.setTitle("Perhatian");
                 builder.setMessage("Selamat Anda Berhasil Daftar, silahkan masukkan profil anda")
                 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog, int id) {

                 }
                 });
                 // Create the AlertDialog object and return it
                 AlertDialog dialog = builder.create();
                 dialog.show(); **/
                finish();


            }
        };
    }

    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    // jika tidak ada jaringan
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_user)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username, password dan Imei.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_server)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_jaringan)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputProfileActivity.this);
                    builder.setTitle("Gagal");
                    builder.setMessage(R.string.error_url)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();

            }
        };
    }
    /**
    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        MarkerOptions marker = new MarkerOptions().position(latLng);
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mMap.addMarker(marker);
        longitude = latLng.latitude;
        latitude = latLng.longitude;

    } **/

    public boolean validasi() {
        boolean valid = true;


        if (nama_depan.isEmpty()) {
            txt_nama_depan.setError("Masukkan Nama Depan");
            valid = false;
        } else {
            txt_nama_depan.setError(null);
        }

        if (nama_belakang.isEmpty()) {
            txt_nama_belakang_.setError("Masukkan Nama Belakang");
            valid = false;
        } else {
            txt_nama_belakang_.setError(null);
        }

        if (telpon.isEmpty()) {
            txt_telepon.setError("Masukkan Telpon/Whatsapp");
            valid = false;
        } else {
            txt_telepon.setError(null);
        }

        if (alamat.isEmpty()) {
            txt_alamat.setError("Masukkan Alamat");
            valid = false;
        } else {
            txt_alamat.setError(null);
        }

        if (tempat_lahir.isEmpty()) {
            txt_tpt_lahir.setError("Masukkan Tempat Lahir");
            valid = false;
        } else {
            txt_tpt_lahir.setError(null);
        }

        if (tanggal_lahir.isEmpty()) {
            txt_tgl_lahir.setError("Masukkan Tanggal Lahir");
            valid = false;
        } else {
            txt_tgl_lahir.setError(null);
        }


        return valid;
    }


}
