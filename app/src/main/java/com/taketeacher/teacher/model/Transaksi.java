package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 07/12/2017.
 */

public class Transaksi implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("tanggal")  private String tanggal;
    @SerializedName("guru_id")  private int guru_id;
    @SerializedName("murid_id")  private int murid_id;
    @SerializedName("status") private int status;
    @SerializedName("tanggal_belajar") private String tanggal_belajar;
    @SerializedName("jumlah_jam")  private int jumlah_jam;
    @SerializedName("alamat")  private String alamat;
    @SerializedName("lat")  private String lat;
    @SerializedName("lng") private String lng;
    @SerializedName("tarif") private int tarif;
    @SerializedName("total_tarif") private int total_tarif;
    @SerializedName("kategori_id") private int kategori_id;
    @SerializedName("subkategori_id") private int subkategori_id;
    @SerializedName("pelajaran_id") private int pelajaran_id;
    @SerializedName("guru")  private Guru guru;
    @SerializedName("pelajaran")  private Pelajaran pelajaran;
    @SerializedName("murid")  private ProfilMurid murid;
    @SerializedName("kategori")  private Kategori kategori;
    @SerializedName("subKategori")  private SubKategori subKategori;
    @SerializedName("items") private List<Transaksi> transaksiList;

    public List<Transaksi> getTransaksiList() {return transaksiList; }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getGuru_id() {
        return guru_id;
    }

    public void setGuru_id(int guru_id) {
        this.guru_id = guru_id;
    }

    public int getMurid_id() {
        return murid_id;
    }

    public void setMurid_id(int murid_id) {
        this.murid_id = murid_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTanggal_belajar() {
        return tanggal_belajar;
    }

    public void setTanggal_belajar(String tanggal_belajar) {
        this.tanggal_belajar = tanggal_belajar;
    }

    public int getJumlah_jam() {
        return jumlah_jam;
    }

    public void setJumlah_jam(int jumlah_jam) {
        this.jumlah_jam = jumlah_jam;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public int getTarif() {
        return tarif;
    }

    public void setTarif(int tarif) {
        this.tarif = tarif;
    }

    public int getTotal_tarif() {
        return total_tarif;
    }

    public void setTotal_tarif(int total_tarif) {
        this.total_tarif = total_tarif;
    }

    public int getKategori_id() {
        return kategori_id;
    }

    public void setKategori_id(int kategori_id) {
        this.kategori_id = kategori_id;
    }

    public int getSubkategori_id() {
        return subkategori_id;
    }

    public void setSubkategori_id(int subkategori_id) {
        this.subkategori_id = subkategori_id;
    }

    public int getPelajaran_id() {
        return pelajaran_id;
    }

    public void setPelajaran_id(int pelajaran_id) {
        this.pelajaran_id = pelajaran_id;
    }

    public Guru getGuru() {
        return guru;
    }

    public void setGuru(Guru guru) {
        this.guru = guru;
    }

    public Pelajaran getPelajaran() {
        return pelajaran;
    }

    public void setPelajaran(Pelajaran pelajaran) {
        this.pelajaran = pelajaran;
    }

    public ProfilMurid getMurid() {
        return murid;
    }

    public void setMurid(ProfilMurid murid) {
        this.murid = murid;
    }

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public SubKategori getSubKategori() {
        return subKategori;
    }

    public void setSubKategori(SubKategori subKategori) {
        this.subKategori = subKategori;
    }
}
