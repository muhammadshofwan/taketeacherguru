package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shofwan-PC on 12/21/2017.
 */

public class User {
    @SerializedName("status") private int status;
    @SerializedName("username")  private String username;
    @SerializedName("password")  private String password;
    @SerializedName("id")  private int id;
    @SerializedName("token")  private String token;
    @SerializedName("auth_key")  private String auth_key;
    @SerializedName("password_hash")  private String password_hash;
    @SerializedName("confirmation_token")  private String confirmation_token;
    @SerializedName("created_at")  private int created_at;
    @SerializedName("updated_at")  private int updated_at;
    @SerializedName("registration_ip")  private String registration_ip;
    @SerializedName("bind_to_ip")  private String bind_to_ip;
    @SerializedName("email")  private String email;
    @SerializedName("email_confirmed")  private int email_confirmed;
    @SerializedName("emailAuthHash")  private String emailAuthHash;

    public String getEmailAuthHash() {
        return emailAuthHash;
    }

    public void setEmailAuthHash(String emailAuthHash) {
        this.emailAuthHash = emailAuthHash;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public void setPassword_hash(String password_hash) {
        this.password_hash = password_hash;
    }

    public String getConfirmation_token() {
        return confirmation_token;
    }

    public void setConfirmation_token(String confirmation_token) {
        this.confirmation_token = confirmation_token;
    }

    public int getCreated_at() {
        return created_at;
    }

    public void setCreated_at(int created_at) {
        this.created_at = created_at;
    }

    public int getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(int updated_at) {
        this.updated_at = updated_at;
    }

    public String getRegistration_ip() {
        return registration_ip;
    }

    public void setRegistration_ip(String registration_ip) {
        this.registration_ip = registration_ip;
    }

    public String getBind_to_ip() {
        return bind_to_ip;
    }

    public void setBind_to_ip(String bind_to_ip) {
        this.bind_to_ip = bind_to_ip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEmail_confirmed() {
        return email_confirmed;
    }

    public void setEmail_confirmed(int email_confirmed) {
        this.email_confirmed = email_confirmed;
    }
}
