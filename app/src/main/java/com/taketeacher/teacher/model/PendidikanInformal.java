package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 17/02/2018.
 */

public class PendidikanInformal implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("guru_id")  private int guru_id;
    @SerializedName("nama_kursus")  private String tahun_selesai;
    @SerializedName("keterangan") private String tahun_mulai;
    @SerializedName("nama_tempat") private String nama_tempat;
    @SerializedName("tanggal_mulai")  private String tanggal_mulai;
    @SerializedName("tanggal_selesai")  private String tanggal_selesai;
    @SerializedName("items") private List<PendidikanInformal> pendidikanInformalList;

    public List<PendidikanInformal> getPendidikanInformalList() {return pendidikanInformalList; }


}
