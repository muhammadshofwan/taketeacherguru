package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 07/12/2017.
 */

public class KetersediaanGuru implements Serializable {
    @SerializedName("id") private String id;
    @SerializedName("guru_id")  private String guru_id;
    @SerializedName("tanggal")  private String tanggal;
    @SerializedName("waktu_mulai") private String waktu_mulai;
    @SerializedName("waktu_selesai") private String waktu_selesai;
    @SerializedName("status")  private String status;
    @SerializedName("keterangan")  private String keterangan;
    @SerializedName("items") private List<KetersediaanGuru> ketersediaanGuruList;

    public List<KetersediaanGuru> getKetersediaanGuruList() {
        return ketersediaanGuruList;
    }

    public void setKetersediaanGuruList(List<KetersediaanGuru> ketersediaanGuruList) {
        this.ketersediaanGuruList = ketersediaanGuruList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuru_id() {
        return guru_id;
    }

    public void setGuru_id(String guru_id) {
        this.guru_id = guru_id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu_mulai() {
        return waktu_mulai;
    }

    public void setWaktu_mulai(String waktu_mulai) {
        this.waktu_mulai = waktu_mulai;
    }

    public String getWaktu_selesai() {
        return waktu_selesai;
    }

    public void setWaktu_selesai(String waktu_selesai) {
        this.waktu_selesai = waktu_selesai;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }


}
