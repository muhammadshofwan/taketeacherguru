package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DELL on 10/12/2017.
 */

public class Mengajar {
    @SerializedName("id") private int id;
    @SerializedName("guru_id")  private int guru_id;
    @SerializedName("pelajaran_id")  private int pelajaran_id;
    @SerializedName("nama_pelajaran")  private String nama_pelajaran;
    @SerializedName("jenjang") private String jenjang;
    @SerializedName("guru")  private Guru guru;
    @SerializedName("items") private List<Mengajar> mengajarList;

    public List<Mengajar> getMengajarList() {return mengajarList; }


    public Guru getGuru() {
        return guru;
    }

    public void setGuru(Guru guru) {
        this.guru = guru;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGuru_id() {
        return guru_id;
    }

    public void setGuru_id(int guru_id) {
        this.guru_id = guru_id;
    }

    public int getPelajaran_id() {
        return pelajaran_id;
    }

    public void setPelajaran_id(int pelajaran_id) {
        this.pelajaran_id = pelajaran_id;
    }

    public String getNama_pelajaran() {
        return nama_pelajaran;
    }

    public void setNama_pelajaran(String nama_pelajaran) {
        this.nama_pelajaran = nama_pelajaran;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }
}
