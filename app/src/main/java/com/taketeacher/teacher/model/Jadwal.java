package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import com.taketeacher.teacher.utils.Link;
import com.taketeacher.teacher.utils.Meta;

/**
 * Created by DELL on 07/12/2017.
 */

public class Jadwal implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("murid_id")  private int murid_id;
    @SerializedName("nama_murid")  private String nama_murid;
    @SerializedName("tanggal")  private String tanggal;
    @SerializedName("pelajaran_id") private int pelajaran_id;
    @SerializedName("nama_pelajaran") private String nama_pelajaran;
    @SerializedName("guru_id")  private int guru_id;
    @SerializedName("durasi")  private int durasi;
    @SerializedName("lokasi")  private String lokasi;
    @SerializedName("keterangan")  private String keterangan;
    @SerializedName("acc_guru") private int acc_guru;
    @SerializedName("acc_murid") private int acc_murid;
    @SerializedName("status") private int status;
    @SerializedName("tanggal_acc_murid")  private String tanggal_acc_murid;
    @SerializedName("tanggal_acc_guru")  private String tanggal_acc_guru;
    @SerializedName("status_label")  private String status_label;
    @SerializedName("ratingGuru")  Rating ratingGuru;
    @SerializedName("longitude")  private String longitude;
    @SerializedName("latitude")  private String latitude;
    @SerializedName("alasan_reschedule_murid")  private String alasan_reschedule_murid;
    @SerializedName("alasan_reschedule_guru")  private String alasan_reschedule_guru;
    @SerializedName("guru")  private Guru guru;
    @SerializedName("murid")  private ProfilMurid murid;
    @SerializedName("items") private List<Jadwal> jadwalList;
    @SerializedName("_links") private Link link;
    @SerializedName("_meta") private Meta meta;

    public int getDurasi() {
        return durasi;
    }

    public void setDurasi(int durasi) {
        this.durasi = durasi;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Rating getRatingGuru() {
        return ratingGuru;
    }

    public void setRatingGuru(Rating ratingGuru) {
        this.ratingGuru = ratingGuru;
    }

    public List<Jadwal> getJadwalList() {return jadwalList; }
    public int getId() {
        return id;
    }

    public String getStatus_label() {
        return status_label;
    }

    public void setStatus_label(String status_label) {
        this.status_label = status_label;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMurid_id() {
        return murid_id;
    }

    public void setMurid_id(int murid_id) {
        this.murid_id = murid_id;
    }

    public String getNama_murid() {
        return nama_murid;
    }

    public void setNama_murid(String nama_murid) {
        this.nama_murid = nama_murid;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getPelajaran_id() {
        return pelajaran_id;
    }

    public void setPelajaran_id(int pelajaran_id) {
        this.pelajaran_id = pelajaran_id;
    }

    public String getNama_pelajaran() {
        return nama_pelajaran;
    }

    public void setNama_pelajaran(String nama_pelajaran) {
        this.nama_pelajaran = nama_pelajaran;
    }

    public int getGuru_id() {
        return guru_id;
    }

    public void setGuru_id(int guru_id) {
        this.guru_id = guru_id;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public int getAcc_guru() {
        return acc_guru;
    }

    public void setAcc_guru(int acc_guru) {
        this.acc_guru = acc_guru;
    }

    public int getAcc_murid() {
        return acc_murid;
    }

    public void setAcc_murid(int acc_murid) {
        this.acc_murid = acc_murid;
    }

    public String getTanggal_acc_murid() {
        return tanggal_acc_murid;
    }

    public void setTanggal_acc_murid(String tanggal_acc_murid) {
        this.tanggal_acc_murid = tanggal_acc_murid;
    }

    public String getTanggal_acc_guru() {
        return tanggal_acc_guru;
    }

    public void setTanggal_acc_guru(String tanggal_acc_guru) {
        this.tanggal_acc_guru = tanggal_acc_guru;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Guru getGuru() {
        return guru;
    }

    public void setGuru(Guru guru) {
        this.guru = guru;
    }

    public ProfilMurid getMurid() {
        return murid;
    }

    public void setMurid(ProfilMurid murid) {
        this.murid = murid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlasan_reschedule_murid() {
        return alasan_reschedule_murid;
    }

    public void setAlasan_reschedule_murid(String alasan_reschedule_murid) {
        this.alasan_reschedule_murid = alasan_reschedule_murid;
    }

    public String getAlasan_reschedule_guru() {
        return alasan_reschedule_guru;
    }

    public void setAlasan_reschedule_guru(String alasan_reschedule_guru) {
        this.alasan_reschedule_guru = alasan_reschedule_guru;
    }
}
