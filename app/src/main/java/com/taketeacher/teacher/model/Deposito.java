package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import com.taketeacher.teacher.utils.Link;
import com.taketeacher.teacher.utils.Meta;

/**
 * Created by DELL on 07/12/2017.
 */

public class Deposito {
    @SerializedName("id") private int id;
    @SerializedName("tanggal")  private String tanggal;
    @SerializedName("murid_id")  private int murid_id;
    @SerializedName("jumlah")  private int jumlah;
    @SerializedName("metode") private int metode;
    @SerializedName("status") private int status;
    @SerializedName("tanggal_verifikasi")  private String tanggal_verifikasi;
    @SerializedName("user_verifikasi")  private int user_verifikasi;
    @SerializedName("keterangan")  private String keterangan;
    @SerializedName("items") private List<Deposito> depositoList;
    @SerializedName("_links") private Link link;
    @SerializedName("_meta") private Meta meta;



    public List<Deposito> getDepositoList() {return depositoList; }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getMurid_id() {
        return murid_id;
    }

    public void setMurid_id(int murid_id) {
        this.murid_id = murid_id;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int getMetode() {
        return metode;
    }

    public void setMetode(int metode) {
        this.metode = metode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTanggal_verifikasi() {
        return tanggal_verifikasi;
    }

    public void setTanggal_verifikasi(String tanggal_verifikasi) {
        this.tanggal_verifikasi = tanggal_verifikasi;
    }

    public int getUser_verifikasi() {
        return user_verifikasi;
    }

    public void setUser_verifikasi(int user_verifikasi) {
        this.user_verifikasi = user_verifikasi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }


}
