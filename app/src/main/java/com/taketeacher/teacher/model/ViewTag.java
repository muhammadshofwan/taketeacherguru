package com.taketeacher.teacher.model;

/**
 * Created by DELL on 27/01/2018.
 */

public class ViewTag {
    private int id;
    private String conditionKey;
    private String conditionValue;
    private String name;
    private Boolean hidden;
    private int required;
    private Boolean filled;
    private int realRequired;
    private int currentRequired;

    @Override
    public String toString() {
        return Integer.toString(this.id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConditionKey() {
        return conditionKey;
    }

    public void setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public Boolean getFilled() {
        return filled;
    }

    public void setFilled(Boolean filled) {
        this.filled = filled;
    }

    public int getRealRequired() {
        return realRequired;
    }

    public void setRealRequired(int realRequired) {
        this.realRequired = realRequired;
    }

    public int getCurrentRequired() {
        return currentRequired;
    }

    public void setCurrentRequired(int currentRequired) {
        this.currentRequired = currentRequired;
    }
}
