package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import com.taketeacher.teacher.utils.Link;
import com.taketeacher.teacher.utils.Meta;

/**
 * Created by shofwan-PC on 12/27/2017.
 */

public class SubKategori implements Serializable {

    @SerializedName("id") private int id;
    @SerializedName("nama")  private String nama;
    @SerializedName("kategoriId")  private int kategoriId;
    @SerializedName("gambarUrl")  private String gambarUrl;
    @SerializedName("status")  private int status;
    @SerializedName("created_at")  private String created_at;
    @SerializedName("update_at")  private String update_at;
    @SerializedName("items") private List<SubKategori> subKategori;
    @SerializedName("pelajaran") private List<Pelajaran> pelajaran;
    @SerializedName("_links") private Link link;
    @SerializedName("_meta") private Meta meta;

    public List<SubKategori> getSubKategori() {return subKategori; }

    public List<Pelajaran> getPelajaran() {return pelajaran; }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getKategoriId() {
        return kategoriId;
    }

    public void setKategoriId(int kategoriId) {
        this.kategoriId = kategoriId;
    }

    public String getGambarUrl() {
        return gambarUrl;
    }

    public void setGambarUrl(String gambarUrl) {
        this.gambarUrl = gambarUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
}
