package com.taketeacher.teacher.model;

/**
 * Created by DELL on 07/12/2017.
 */

public class MainMenu {
    private int image;
    private String teks;

    public MainMenu() {
    }

    public MainMenu(String teks, int image) {
        this.teks = teks;
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTeks() {
        return teks;
    }

    public void setTeks(String teks) {
        this.teks = teks;
    }
}
