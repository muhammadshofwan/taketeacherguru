package com.taketeacher.teacher.model;

/**
 * Created by DELL on 07/12/2017.
 */

public class Deposit {
    private int image;
    private int id;
    private String title;
    private String nominal;
    private String tanggal;
    private String jam;

    public Deposit() {
    }

    public Deposit(int id, String title, String nominal, int image, String pelajaran, String status, String tanggal, String jam) {
        this.id = id;
        this.title = title;
        this.nominal = nominal;
        this.image = image;
        this.tanggal = tanggal;
        this.jam = jam;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }
}
