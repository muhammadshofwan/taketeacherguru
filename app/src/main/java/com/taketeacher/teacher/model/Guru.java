package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 10/12/2017.
 */

@SuppressWarnings("serial")
public class Guru  implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("title_depan")  private String title_depan;
    @SerializedName("titel_belakang")  private String titel_belakang;
    @SerializedName("nama_depan")  private String nama_depan;
    @SerializedName("nama_belakang")  private String nama_belakang;
    @SerializedName("namaLengkap")  private String namaLengkap;
    @SerializedName("alamat")  private String alamat;
    @SerializedName("tempat_lahir")  private String tempat_lahir;
    @SerializedName("email")  private String email;
    @SerializedName("telpon")  private String telpon;
    @SerializedName("tanggal_lahir")  private String tanggal_lahir;
    @SerializedName("jenis_kelamin") private int jenis_kelamin;
    @SerializedName("jenis_kelamin_label") private String jenis_kelamin_label;
    @SerializedName("lat")  private String lat;
    @SerializedName("lng")  private String lng;
    @SerializedName("basecamp_id")  private int basecamp_id;
    @SerializedName("agama_label")  private String agama_label;
    @SerializedName("agama")  private int agama;
    @SerializedName("items") private List<Guru> gurus;
    @SerializedName("guruPendidikanFormals")  private List<PendidikanFormal> guruPendidikanFormals;;
    @SerializedName("guruPendidikanNonFormals")  private List<PendidikanInformal> guruPendidikanNonFormals;
    @SerializedName("nama_basecamp")  private String nama_basecamp;
    @SerializedName("image_url")  private String image_url;
    @SerializedName("rating") private String rating;
    @SerializedName("saldo") private int saldo;
    @SerializedName("ratings")  private List<Rating> ratings;

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public List<Rating> getRatings() {
        return ratings;
    }


    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<Guru> getGurus() {return gurus; }

    public int getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(int jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getAgama_label() {
        return agama_label;
    }

    public void setAgama_label(String agama_label) {
        this.agama_label = agama_label;
    }

    public int getAgama() {
        return agama;
    }

    public void setAgama(int agama) {
        this.agama = agama;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getJenis_kelamin_label() {
        return jenis_kelamin_label;
    }

    public void setJenis_kelamin_label(String jenis_kelamin_label) {
        this.jenis_kelamin_label = jenis_kelamin_label;
    }

    public String getTitle_depan() {
        return title_depan;
    }

    public void setTitle_depan(String title_depan) {
        this.title_depan = title_depan;
    }

    public String getTitel_belakang() {
        return titel_belakang;
    }

    public void setTitel_belakang(String titel_belakang) {
        this.titel_belakang = titel_belakang;
    }

    public String getNama_belakang() {
        return nama_belakang;
    }

    public void setNama_belakang(String nama_belakang) {
        this.nama_belakang = nama_belakang;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_depan() {
        return nama_depan;
    }

    public void setNama_depan(String nama_depan) {
        this.nama_depan = nama_depan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    public String getNama_basecamp() {
        return nama_basecamp;
    }

    public void setNama_basecamp(String nama_basecamp) {
        this.nama_basecamp = nama_basecamp;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public int getBasecamp_id() {
        return basecamp_id;
    }

    public void setBasecamp_id(int basecamp_id) {
        this.basecamp_id = basecamp_id;
    }

    public List<PendidikanInformal> getGuruPendidikanNonFormals() {
        return guruPendidikanNonFormals;
    }

    public void setGuruPendidikanNonFormals(List<PendidikanInformal> guruPendidikanNonFormals) {
        this.guruPendidikanNonFormals = guruPendidikanNonFormals;
    }

    public List<PendidikanFormal> getGuruPendidikanFormals() {
        return guruPendidikanFormals;
    }

    public void setGuruPendidikanFormals(List<PendidikanFormal> guruPendidikanFormals) {
        this.guruPendidikanFormals = guruPendidikanFormals;

    }
}
