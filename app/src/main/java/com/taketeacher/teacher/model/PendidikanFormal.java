package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 17/02/2018.
 */

public class PendidikanFormal implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("jenjang")  private int jenjang;
    @SerializedName("guru_id")  private int guru_id;
    @SerializedName("tahun_selesai")  private int tahun_selesai;
    @SerializedName("tahun_mulai") private int tahun_mulai;
    @SerializedName("nama_tempat") private String nama_tempat;
    @SerializedName("jurusan")  private String jurusan;
    @SerializedName("status")  private int status;
    @SerializedName("items") private List<PendidikanFormal> pendidikanFormalList;

    public List<PendidikanFormal> getPendidikanFormalList() {return pendidikanFormalList; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getJenjang() {
        return jenjang;
    }

    public void setJenjang(int jenjang) {
        this.jenjang = jenjang;
    }

    public int getGuru_id() {
        return guru_id;
    }

    public void setGuru_id(int guru_id) {
        this.guru_id = guru_id;
    }

    public int getTahun_selesai() {
        return tahun_selesai;
    }

    public void setTahun_selesai(int tahun_selesai) {
        this.tahun_selesai = tahun_selesai;
    }

    public int getTahun_mulai() {
        return tahun_mulai;
    }

    public void setTahun_mulai(int tahun_mulai) {
        this.tahun_mulai = tahun_mulai;
    }

    public String getNama_tempat() {
        return nama_tempat;
    }

    public void setNama_tempat(String nama_tempat) {
        this.nama_tempat = nama_tempat;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
