package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;
import com.taketeacher.teacher.utils.Link;
import com.taketeacher.teacher.utils.Meta;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 07/12/2017.
 */

public class Pelajaran implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("jenjang_id") private int jenjang_id;
    @SerializedName("nama") private String nama;
    @SerializedName("status") private String status;
    @SerializedName("image_url") private String image_url;
    @SerializedName("items") private List<Pelajaran> pelajaran;
    @SerializedName("guru") private List<Guru> guru;
    @SerializedName("_links") private Link link;
    @SerializedName("_meta") private Meta meta;

    public Pelajaran() {}

    public List<Guru> getguru() {return guru; }

    public List<Pelajaran> getPelajaran() {return pelajaran; }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getJenjang_id() {
        return jenjang_id;
    }

    public void setJenjang_id(int jenjang_id) {
        this.jenjang_id = jenjang_id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
