package com.taketeacher.teacher.model;

/**
 * Created by DELL on 07/12/2017.
 */

public class Pendidikan {
    private int image;
    private int id;
    private String title;
    private String tahun;

    public Pendidikan() {
    }

    public Pendidikan(int id, String title, int image, String tahun) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.tahun = tahun;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }
}
