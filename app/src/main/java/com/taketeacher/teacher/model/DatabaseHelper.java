package com.taketeacher.teacher.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "admin_sapu.db";
	private static final String CREATE_TABLE_CLIENT=
		"CREATE TABLE client (" + 
			"id INTEGER PRIMARY KEY ," + 
			"nama TEXT," + 
			"alamat TEXT," + 
			"hp TEXT," + 
			"email TEXT" + 
		");" ; 
	private static final String CREATE_TABLE_JENIS_PAKET=
		"CREATE TABLE jenis_paket (" + 
			"id INTEGER PRIMARY KEY ," + 
			"nama TEXT," + 
			"keterangan TEXT" + 
		");" ; 
	private static final String CREATE_TABLE_ORDER=
		"CREATE TABLE order (" + 
			"id INTEGER PRIMARY KEY ," + 
			"tanggal_order NUMERIC," + 
			"client INTEGER," + 
			"paket INTEGER," + 
			"tarif INTEGER," + 
			"tambahan TEXT," + 
			"hp TEXT," + 
			"tanggal_pengerjaan NUMERIC," + 
			"status INTEGER," + 
			"keterangan TEXT" + 
		");" ; 
	private static final String CREATE_TABLE_ORDER_PETUGAS=
		"CREATE TABLE order_petugas (" + 
			"id INTEGER PRIMARY KEY ," + 
			"id_order INTEGER," + 
			"id_petugas INTEGER" + 
		");" ; 
	private static final String CREATE_TABLE_ORDER_PRIORITAS=
		"CREATE TABLE order_prioritas (" + 
			"id INTEGER PRIMARY KEY ," + 
			"id_order INTEGER," + 
			"prioritas TEXT" + 
		");" ; 
	private static final String CREATE_TABLE_PAKET=
		"CREATE TABLE paket (" + 
			"id INTEGER PRIMARY KEY ," + 
			"nama TEXT," + 
			"tarif INTEGER," + 
			"keterangan TEXT," + 
			"jenis INTEGER" + 
		");" ; 
	private static final String CREATE_TABLE_PENGGUNA=
		"CREATE TABLE pengguna (" + 
			"id INTEGER PRIMARY KEY ," + 
			"username TEXT," + 
			"password TEXT," + 
			"email TEXT," + 
			"level TEXT," + 
			"kode TEXT," + 
			"auth_key TEXT," + 
			"nama TEXT" + 
		");" ; 
	private static final String CREATE_TABLE_PETUGAS=
		"CREATE TABLE petugas (" + 
			"id INTEGER PRIMARY KEY ," + 
			"nama TEXT," + 
			"keterangan TEXT," + 
			"rating INTEGER," + 
			"gambar TEXT" + 
		");" ; 
	private static final String CREATE_TABLE_PRIORITAS=
		"CREATE TABLE prioritas (" + 
			"nama TEXT PRIMARY KEY " + 
		");" ; 
	public DatabaseHelper(Context context) { super(context, DATABASE_NAME, null, 1); }
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_CLIENT);
		db.execSQL(CREATE_TABLE_JENIS_PAKET);
		db.execSQL(CREATE_TABLE_ORDER);
		db.execSQL(CREATE_TABLE_ORDER_PETUGAS);
		db.execSQL(CREATE_TABLE_ORDER_PRIORITAS);
		db.execSQL(CREATE_TABLE_PAKET);
		db.execSQL(CREATE_TABLE_PENGGUNA);
		db.execSQL(CREATE_TABLE_PETUGAS);
		db.execSQL(CREATE_TABLE_PRIORITAS);


	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS client");
		db.execSQL("DROP TABLE IF EXISTS jenis_paket");
		db.execSQL("DROP TABLE IF EXISTS order");
		db.execSQL("DROP TABLE IF EXISTS order_petugas");
		db.execSQL("DROP TABLE IF EXISTS order_prioritas");
		db.execSQL("DROP TABLE IF EXISTS paket");
		db.execSQL("DROP TABLE IF EXISTS pengguna");
		db.execSQL("DROP TABLE IF EXISTS petugas");
		db.execSQL("DROP TABLE IF EXISTS prioritas");


		onCreate(db);
	}
}

