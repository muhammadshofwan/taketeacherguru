package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import com.taketeacher.teacher.utils.Link;
import com.taketeacher.teacher.utils.Meta;

/**
 * Created by DELL on 07/12/2017.
 */

public class DepositMutasi implements Serializable{
    @SerializedName("id") private int id;
    @SerializedName("tanggal")  private String tanggal;
    @SerializedName("user_id")  private int user_id;
    @SerializedName("jenis")  private String jenis;
    @SerializedName("masuk") private int masuk;
    @SerializedName("keluar") private int keluar;
    @SerializedName("keterangan")  private String keterangan;
    @SerializedName("items") private List<DepositMutasi> depositMutasiList;
    @SerializedName("_links") private Link link;
    @SerializedName("_meta") private Meta meta;

    public List<DepositMutasi> getDepositMutasiList() {
        return depositMutasiList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public int getMasuk() {
        return masuk;
    }

    public void setMasuk(int masuk) {
        this.masuk = masuk;
    }

    public int getKeluar() {
        return keluar;
    }

    public void setKeluar(int keluar) {
        this.keluar = keluar;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
