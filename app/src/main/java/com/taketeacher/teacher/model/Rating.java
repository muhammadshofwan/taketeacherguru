package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Rating implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("user_id") private int user_id;
    @SerializedName("voter_id") private int voter_id;
    @SerializedName("point") private int point;
    @SerializedName("jenis") private int jenis;
    @SerializedName("tanggal") private String tanggal;
    @SerializedName("komentar") private String komentar;
    @SerializedName("voter_name") private String voter_name;
    @SerializedName("user_name") private String user_name;
    @SerializedName("voter_image") private String voter_image;
    @SerializedName("user_image") private String user_image;
    @SerializedName("jadwal_id") private int jadwal_id;
    @SerializedName("items") private List<Rating> ratingList;
    @SerializedName("voter")  private ProfilMurid murid;
    //@SerializedName("voter")  private User user;

    public List<Rating> getRatingList() {return ratingList; }

    public String getVoter_image() {
        return voter_image;
    }

    public void setVoter_image(String voter_image) {
        this.voter_image = voter_image;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getVoter_name() {
        return voter_name;
    }

    public void setVoter_name(String voter_name) {
        this.voter_name = voter_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getJadwal_id() {
        return jadwal_id;
    }

    public void setJadwal_id(int jadwal_id) {
        this.jadwal_id = jadwal_id;
    }

    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getVoter_id() {
        return voter_id;
    }

    public void setVoter_id(int voter_id) {
        this.voter_id = voter_id;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public ProfilMurid getMurid() {
        return murid;
    }

    public void setMurid(ProfilMurid murid) {
        this.murid = murid;
    }

  //  public User getUser() {
    //    return user;
    //}

    //public void setUser(User user) {
      //  this.user = user;
    //}
}
