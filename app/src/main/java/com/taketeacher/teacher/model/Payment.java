package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;
import com.taketeacher.teacher.utils.Link;
import com.taketeacher.teacher.utils.Meta;

import java.util.List;

/**
 * Created by DELL on 10/12/2017.
 */

public class Payment {
    @SerializedName("id") private int id;
    @SerializedName("nama")  private String nama;
    @SerializedName("status")  private int status;
    @SerializedName("created_at")  private String created_at;
    @SerializedName("userAdd") private String userAdd;
    @SerializedName("updated_at")  private String updated_at;
    @SerializedName("userUpdate")  private String userUpdate;
    @SerializedName("items") private List<Payment> paymentList;
    @SerializedName("_links") private Link link;
    @SerializedName("_meta") private Meta meta;

    @Override
    public String toString() {
        return this.nama;
    }

    public List<Payment> getPaymentList() {return paymentList; }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUserAdd() {
        return userAdd;
    }

    public void setUserAdd(String userAdd) {
        this.userAdd = userAdd;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }
}
