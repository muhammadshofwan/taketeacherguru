package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 10/12/2017.
 */

public class ProfilMurid implements Serializable {
    @SerializedName("id") private int id;
    @SerializedName("nama_depan")  private String nama_depan;
    @SerializedName("nama_belakang")  private String nama_belakang;
    @SerializedName("namaLengkap")  private String namaLengkap;
    @SerializedName("telpon")  private String telpon;
    @SerializedName("alamat")  private String alamat;
    @SerializedName("longitude") private String longitude;
    @SerializedName("latitude")  private String latitude;
    @SerializedName("email")  private String email;
    @SerializedName("tempat_lahir") private String tempat_lahir;
    @SerializedName("tanggal_lahir") private String tanggal_lahir;
    @SerializedName("jenis_kelamin") private String jenis_kelamin;
    @SerializedName("image") private String image;
    @SerializedName("user_id") private int user_id;
    @SerializedName("agama_label") private String agama_label;
    @SerializedName("saldo") private int saldo;
    @SerializedName("jenis_kelamin_label") private String jenis_kelamin_label;
    @SerializedName("image_url") private String image_url;

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_depan() {
        return nama_depan;
    }

    public void setNama_depan(String nama_depan) {
        this.nama_depan = nama_depan;
    }

    public String getNama_belakang() {
        return nama_belakang;
    }

    public void setNama_belakang(String nama_belakang) {
        this.nama_belakang = nama_belakang;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getAgama_label() {
        return agama_label;
    }

    public void setAgama_label(String agama_label) {
        this.agama_label = agama_label;
    }

    public String getJenis_kelamin_label() {
        return jenis_kelamin_label;
    }

    public void setJenis_kelamin_label(String jenis_kelamin_label) {
        this.jenis_kelamin_label = jenis_kelamin_label;
    }
}
