package com.taketeacher.teacher.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 10/12/2017.
 */

public class Basecamp {
    @SerializedName("id") private int id;
    @SerializedName("nama")  private String nama;
    @SerializedName("alamat")  private String alamat;
    @SerializedName("telpon")  private String telpon;
    @SerializedName("longitude") private String longitude;
    @SerializedName("latitude")  private String latitude;
    @SerializedName("status")  private int status;
    @SerializedName("harga") private int harga;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
}
