package com.taketeacher.teacher;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.taketeacher.teacher.model.KetersediaanGuru;
import com.taketeacher.teacher.utils.AppController;
import com.taketeacher.teacher.utils.GsonRequest;
import com.taketeacher.teacher.utils.Preference;

public class KalenderActivity extends AppCompatActivity {
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMMM - yyyy", Locale.getDefault());
    KetersediaanGuru ketersediaanGuru;
    String tanggal_awal;
    String tanggal_akhir;
    String tanggal_start;
    String tanggal_finish;
    CompactCalendarView compactCalendarView;
    ImageButton btn_prev, btn_next;
    TextView txt_month;
    List<String> mutableEvents = new ArrayList<>();
    ListView listView;
    ArrayAdapter adapter;
    Date selected;
    private String guru_id;
    TextView lbl_info;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalender);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            guru_id = extras.getString("guru_id");
        }
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        // Set first day of week to Monday, defaults to Monday so calling setFirstDayOfWeek is not necessary
        // Use constants provided by Java Calendar class
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        progress = new ProgressDialog(this);
        //  selected = new Date();
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = cal.getTime();

        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DATE, -1);
        Date lastDayOfMonth = cal.getTime();

        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        tanggal_awal = sdf.format(firstDayOfMonth);
        tanggal_akhir = sdf.format(lastDayOfMonth);

        txt_month = (TextView) findViewById(R.id.txt_month);
        txt_month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));


        btn_prev = (ImageButton) findViewById(R.id.btn_prev_month);
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollLeft();
            }
        });
        btn_next = (ImageButton) findViewById(R.id.btn_next_month);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollRight();
            }
        });

        adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.list_text_item, mutableEvents);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        //set title on calendar scroll
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                selected = dateClicked;
                List<Event> selectedEvents = compactCalendarView.getEvents(dateClicked);
                if(selectedEvents.size() > 0){
                    lbl_info.setVisibility(View.VISIBLE);
                }else{
                    lbl_info.setVisibility(View.INVISIBLE);
                }
                if (selectedEvents != null) {
                    mutableEvents.clear();
                    for (Event booking : selectedEvents) {
                        ketersediaanGuru = (KetersediaanGuru) booking.getData();
                        mutableEvents.add(ketersediaanGuru.getWaktu_mulai().substring(0,5) +"  sampai dengan  "+ ketersediaanGuru.getWaktu_selesai().substring(0,5));
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                txt_month.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                mutableEvents.clear();
                adapter.notifyDataSetChanged();

                Date today = firstDayOfNewMonth;
                Calendar cal = Calendar.getInstance();
                cal.setTime(today);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                Date firstDayOfMonth = cal.getTime();

                cal.add(Calendar.MONTH, 1);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                cal.add(Calendar.DATE, -1);
                Date lastDayOfMonth = cal.getTime();

                DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                tanggal_awal = sdf.format(firstDayOfMonth);
                tanggal_akhir = sdf.format(lastDayOfMonth);

                downloadListJadwal(guru_id,tanggal_awal,tanggal_akhir, "tanggal");
            }
        });


        FloatingActionButton getDateButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        getDateButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String selectedTanggal = sdf.format(selected).toString();
                returnIntent.putExtra("tanggal",selectedTanggal);
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf1.parse(selectedTanggal));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c.add(Calendar.DATE, 27);  // number of days to add
                tanggal_finish = sdf1.format(c.getTime());
                returnIntent.putExtra("tanggal_finish",tanggal_finish);
                returnIntent.putExtra("tanggal_awal",tanggal_awal);
                returnIntent.putExtra("tanggal_akhir",tanggal_akhir);
                returnIntent.putExtra("guru_id",guru_id);
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        });

        lbl_info = (TextView) findViewById(R.id.lbl_info);
        lbl_info.setVisibility(View.INVISIBLE);

        downloadListJadwal(guru_id,tanggal_awal,tanggal_akhir, "tanggal");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void downloadListJadwal(String guru_id, String tanggalAwal, String tanggalAkhir, String sort){
        Preference pref = new Preference(getApplicationContext());
        //http://app.taketeacher.com/muridapi/jadwal/cekjadwalguru?access-token=z4seNW_yFMaK3tup4Ma_NNSbif56tpTt&id=2&tanggalmulai=2018-09-01&tanggalselesai=2018-09-30
        String host  = "http://app.taketeacher.com/muridapi/jadwal/cekketersediaanguru?";
        host += "access-token=" + pref.getToken();
        host += "&id="+guru_id;
        host += "&tanggalmulai="+tanggalAwal;
        host += "&tanggalselesai="+tanggalAkhir;
        GsonRequest<KetersediaanGuru> jsObjRequest = new GsonRequest<KetersediaanGuru>(
                Request.Method.GET,
                host,
                KetersediaanGuru.class,
                this.downloadDataPelajaranReqSuccessListener(),
                this.downloadDataPelajaranReqErrorListener());
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController controller = new AppController(this.getApplicationContext());
        controller.addToRequestQueue(jsObjRequest);

        progress.setTitle("Loading");
        progress.setMessage("Cek Ketersediaan Guru...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    private Response.Listener<KetersediaanGuru> downloadDataPelajaranReqSuccessListener() {
        return new Response.Listener<KetersediaanGuru>() {
            @Override
            public void onResponse(KetersediaanGuru response) {

                progress.dismiss();
                compactCalendarView.removeAllEvents();
                KetersediaanGuru jadwal = response;
                ketersediaanGuru = jadwal;
                //JadwalAdapter adapter = new JadwalAdapter(ApplandeoActivity.this,response.getJadwalList());

                List<KetersediaanGuru> jadwalList = jadwal.getKetersediaanGuruList();

                //    if (jadwal.getStatus() == 0 || jadwal.getStatus() == 1){
                for (KetersediaanGuru item : jadwal.getKetersediaanGuruList()) {

                    String s = item.getTanggal();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat f_tahun = new SimpleDateFormat("yyyy");
                    SimpleDateFormat f_bulan = new SimpleDateFormat("MM");
                    SimpleDateFormat f_tanggal = new SimpleDateFormat("dd");
                    int tahun,bulan,tanggal;

                    try
                    {
                        Date date = simpleDateFormat.parse(s);
                        tahun = Integer.parseInt(f_tahun.format(date));
                        bulan = Integer.parseInt(f_bulan.format(date));
                        tanggal = Integer.parseInt(f_tanggal.format(date));

                        Calendar event =Calendar.getInstance();
                        event.set(Calendar.YEAR, tahun);
                        event.set(Calendar.MONTH, bulan-1);
                        event.set(Calendar.DAY_OF_MONTH, tanggal);
                        Event ev = new Event(Color.GREEN, event.getTimeInMillis(),item);

                        compactCalendarView.addEvent(ev);
                        // System.out.println("date : "+simpleDateFormat.format(date));
                    } catch (ParseException ex) {
                        System.out.println("Exception "+ex);
                    }

                }
            }
        };
    }
    private Response.ErrorListener downloadDataPelajaranReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorDescription = "Error download data Sub Kategori:\r\n";
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    errorDescription = errorDescription + "Tidak ada koneksi internet.";
                } else if (error instanceof AuthFailureError) {
                    // jika user tidak diizinkan
                    errorDescription = errorDescription + "Login tidak berhasil\r\nSilakan periksa username dan password.\r\n";
                } else if (error instanceof ServerError) {
                    // jika error diserver
                    errorDescription = errorDescription + "Error server.\r\n";
                } else if (error instanceof NetworkError) {
                    // jika jaringan error
                    errorDescription = errorDescription + "Error jaringan.\r\n";
                } else if (error instanceof ParseError) {
                    // jika tidak dapat menemukan alamat
                    errorDescription = errorDescription + "Error parse url.\r\n";
                }

                String body = "";
                if(error.networkResponse != null){
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                errorDescription = errorDescription + body + "\r\n"+ error.getMessage();
                progress.dismiss();
            }
        };
    }


    @Override
    public void onResume() {
        super.onResume();
        txt_month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        // Set to current day on resume to set calendar to latest day
        // toolbar.setTitle(dateFormatForMonth.format(new Date()));
    }
}

